import { useState } from "react";

const useRegister = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [username, setUsername] = useState("");
  const [selectedDate, setSelectedDate] = useState<Date | null>(null);
  const [gender, setGender] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [passwordMismatch, setPasswordMismatch] = useState(false);
  const [showIndicator, setShowIndicator] = useState(false);
  const [registerClicked, setRegisterClicked] = useState(false);
  const [usernameTaken, setUsernameTaken] = useState(false);
  const [emailTaken, setEmailTaken] = useState(false);

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newPassword = e.target.value;
    setPassword(newPassword);
    setShowIndicator(true);
  };

  const handleUsername = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const newUsername = e.target.value;
    try {
      const url = `${process.env.NEXT_PUBLIC_API_HOST}/account/validateUser/${
        newUsername || null
      }`;
      const response = await fetch(url);
      const data = await response.json();
      if (data.data.length > 0) {
        setUsernameTaken(true);
      } else {
        setUsernameTaken(false);
      }
      setUsername(newUsername);
    } catch (error) {}
  };

  const handleEmail = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const newEmail = e.target.value;
    try {
      const url = `${process.env.NEXT_PUBLIC_API_HOST}/account/validateEmail/${
        newEmail || null
      }`;
      const response = await fetch(url);
      const data = await response.json();
      if (data.data.length > 0) {
        setEmailTaken(true);
      } else {
        setEmailTaken(false);
      }
      setEmail(newEmail);
    } catch (error) {
      console.log(error);
    }

    setEmail(newEmail);
  };

  const checkPasswordStrength = (password: string) => {
    if (password.length < 6) {
      return "Weak";
    } else if (password.length < 8) {
      return "Moderate";
    } else if (password.length < 10) {
      return "Strong";
    } else {
      return "Very Strong";
    }
  };

  const passwordStrength = checkPasswordStrength(password);

  const getIndicatorColor = (strength: string): string => {
    switch (strength) {
      case "Weak":
        return "bg-red-500";
      case "Moderate":
        return "bg-yellow-500";
      case "Strong":
        return "bg-orange-500";
      case "Very Strong":
        return "bg-mv-primary-1";
      default:
        return "bg-gray-300";
    }
  };

  const indicatorColor = getIndicatorColor(passwordStrength);

  const handleConfirmPasswordChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const newConfirmPassword = e.target.value;
    setConfirmPassword(newConfirmPassword);
    setShowIndicator(true);
  };

  return {
    firstName,
    setFirstName,
    lastName,
    setLastName,
    username,
    setUsername,
    selectedDate,
    setSelectedDate,
    gender,
    setGender,
    phoneNumber,
    setPhoneNumber,
    email,
    setEmail,
    password,
    setPassword,
    confirmPassword,
    setConfirmPassword,
    passwordMismatch,
    setPasswordMismatch,
    showIndicator,
    handlePasswordChange,
    setRegisterClicked,
    registerClicked,
    checkPasswordStrength,
    handleConfirmPasswordChange,
    handleEmail,
    indicatorColor,
    handleUsername,
    usernameTaken,
    emailTaken,
  };
};

export default useRegister;
