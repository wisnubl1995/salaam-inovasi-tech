export const formatDate = (dateString: any) => {
  const options: any = {
    month: "long",
    day: "numeric",
    year: "numeric",
    hour: "numeric",
    minute: "numeric",
  };
  const date = new Date(dateString);
  return date.toLocaleString("en-US", options);
};

export const formatDateHalf = (dateString: any) => {
  const options: any = {
    month: "long",
    day: "numeric",
    year: "numeric",
  };
  const date = new Date(dateString);
  return date.toLocaleString("en-US", options);
};
