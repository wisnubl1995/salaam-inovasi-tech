import { gql } from "graphql-request";

export const EVENTS = gql`
  query events {
    events(orderBy: publishedAt_DESC) {
      eventTitle
      eventSlug
      eventDescription
      featured
      eventThumbnail {
        url
      }
      eventDate
      eventCategory {
        categoryTitle
        categorySlug
      }
    }
  }
`;

export const CATEGORIES = gql`
  query eventCategories {
    eventCategories {
      categoryTitle
    }
  }
`;

export const ARTICLECATEGORY = gql`
  query articleCategories {
    articleCategories {
      name
      slug
    }
  }
`;

export const ARTICLES = gql`
  query articles {
    articles(orderBy: publishedAt_DESC) {
      title
      slug
      articleCategory {
        name
      }
      thumbnail {
        url
      }
      content {
        text
      }
      featured
      createdAt
    }
  }
`;

export const ARTICLE = gql`
  query article($slug: String!) {
    article(where: { slug: $slug }) {
      title
      slug
      articleCategory {
        name
      }
      thumbnail {
        url
      }
      content {
        text
        html
      }
      createdAt
    }
  }
`;

export const CLIENTS = gql`
  query clients {
    clients {
      clientName
      slug
      clientImage {
        url
      }
    }
  }
`;

export const PROJECT = gql`
  query project($slug: String!) {
    project(where: { slug: $slug }) {
      projectName
      createdAt
      slug
      client {
        clientName
      }
      projectCategory {
        categoryName
      }
      projectDescription
      imageThumbnail {
        url
      }
      additionalImage {
        url
      }
    }
  }
`;

export const PROJECTS = gql`
  query projects {
    projects {
      projectName
      createdAt
      slug
      client {
        clientName
      }
      projectCategory {
        categoryName
      }
      projectDescription
      imageThumbnail {
        url
      }
      additionalImage {
        url
      }
    }
  }
`;

export const PROJECTCATEGORIES = gql`
  query projectCategories {
    projectCategories {
      categoryName
      slug
      createdAt
    }
  }
`;

export const JOBS = gql`
  query jobs {
    jobs {
      jobRole
      jobLevel
      fullTime
      slug
      workingSite
      requirements {
        html
      }
      mainResponsibility {
        html
      }
      benefits {
        html
      }
    }
  }
`;

export const JOB = gql`
  query job($slug: String!) {
    job(where: { slug: $slug }) {
      jobRole
      jobLevel
      fullTime
      slug
      workingSite
      requirements {
        html
      }
      mainResponsibility {
        html
      }
      benefits {
        html
      }
    }
  }
`;

export const CONFIG = gql`
  query config {
    config(where: { id: "clrypyjv4v0m00b1hwf9sdd6t" }) {
      email
      phoneNumber
      logoHeader {
        url
      }
      logoFooter {
        url
      }
      hideNews
      hideCareer
      recaptchaKey
      instagramUrl
      whatsAppUrl
      twitterUrl
      tikTokUrl
      telegramUrl
    }
  }
`;

export const SMTP = gql`
  query smtpConfiguration {
    smtpConfiguration(where: { id: "clsb7vq6tgiaw0bzxb1eiapmb" }) {
      emailUser
      emailPassword
      displayEmailName
      host
      smtpPort
      secure
      cc
    }
  }
`;
