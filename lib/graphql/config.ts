import { api } from "./api";
import { CONFIG } from "./query";

export const getServerSideProps = async () => {
  const { config }: any = await api.request(CONFIG);

  return {
    props: {
      config,
    },
  };
};
