import Homepage from "@/components/pages/Homepage";
import MaintenancePage from "@/components/templates/Maintenance";
import Seo from "@/components/templates/Seo";
import { api } from "@/lib/graphql/api";
import { CLIENTS, CONFIG, PROJECTS } from "@/lib/graphql/query";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const getServerSideProps = async () => {
  const { clients }: any = await api.request(CLIENTS);
  const { projects }: any = await api.request(PROJECTS);
  const { config }: any = await api.request(CONFIG);

  return {
    props: {
      clients,
      projects,
      config,
    },
  };
};

export default function Home({ clients, projects, config }: any) {
  return (
    <>
      <Seo />
      <Homepage
        clients={clients}
        projects={projects}
        logo={config.logoHeader.url}
      />
    </>
  );
}
