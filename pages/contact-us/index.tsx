import Navbar from "@/components/organisms/Navbar";
import HomeFooter from "@/components/templates/HomePage/HomeFooter";
import React, { useState } from "react";
import logo from "@/public/logo-full.png";
import Image from "next/image";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ReCAPTCHA from "react-google-recaptcha";
import Seo from "@/components/templates/Seo";
import { CONFIG } from "@/lib/graphql/query";
import { api } from "@/lib/graphql/api";

type Props = {
  click?: any;
};

export async function getServerSideProps() {
  const { config }: any = await api.request(CONFIG);
  return {
    props: {
      config,
    },
  };
}

const Index = ({ config }: any) => {
  const [loading, setLoading] = useState(false);
  const [recaptchaValue, setRecaptchaValue] = useState<string | null>(null);

  const contactHandler = async (event: any) => {
    event.preventDefault();
    if (!recaptchaValue) {
      toast.error("Re-captcha is Failed");
      return;
    }
    const target = event.target;
    const { email, firstName, lastName, phoneNumber, company, message } =
      target;
    try {
      setLoading(true);
      const response = await fetch("/api/email", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email.value,
          firstName: firstName.value,
          lastName: lastName.value,
          phoneNumber: phoneNumber.value,
          company: company.value,
          message: message.value,
          recaptchaValue,
        }),
      });
      if (response.ok) {
        toast.success("Your message has been sent.", { autoClose: 3000 });
        setLoading(false);
      } else {
        toast.error("Failed to send your message. Please try again.");
        setLoading(false);
      }
    } catch (error) {
      toast.error("INTERNAL ERROR");
    }
  };
  return (
    <>
      <ToastContainer />
      <Seo />
      <Navbar logo={config.logoHeader.url} />
      <div className="isolate bg-white px-6 py-24 sm:py-32 lg:px-8 font-QuicksandRegular">
        <div className="mx-auto max-w-2xl text-center">
          <h2 className="text-3xl font-QuicksandBold tracking-tight text-gray-900 sm:text-4xl">
            Contact Us
          </h2>
          <p className="mt-2 text-lg leading-8 text-gray-600">
            Get in touch with us. Whether you have questions, ideas, or
            collaboration proposals, we welcome the opportunity to connect.
            Reach out today for prompt and personalized assistance.
          </p>
        </div>
        <div className="container mx-auto p-8 mt-10 bg-white rounded shadow-lg grid grid-cols-1 md:grid-cols-2 gap-8">
          <div>
            <form onSubmit={contactHandler} className="mx-auto  max-w-xl ">
              <div className="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">
                <div>
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    First name
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="text"
                      name="firstName"
                      id="firstName"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div>
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Last name
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="text"
                      name="lastName"
                      id="lastName"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div className="sm:col-span-2">
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Company
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="text"
                      name="company"
                      id="company"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div className="sm:col-span-2">
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Email
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="email"
                      name="email"
                      id="email"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div className="sm:col-span-2">
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Phone number
                  </label>
                  <div className="relative mt-2.5">
                    <input
                      type="tel"
                      name="phoneNumber"
                      id="phoneNumber"
                      className="block w-full rounded-md border-0 px-3.5 py-2 pl-20 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div className="sm:col-span-2">
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Message
                  </label>
                  <div className="mt-2.5">
                    <textarea
                      name="message"
                      id="message"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    ></textarea>
                  </div>
                </div>
                <div className="flex gap-x-4 sm:col-span-2"></div>
              </div>
              <div className="mt-2">
                <ReCAPTCHA
                  sitekey={config.recaptchaKey}
                  onChange={(value) => setRecaptchaValue(value)}
                />
              </div>
              <div className="mt-2">
                <button
                  type="submit"
                  className="w-auto px-5 py-1 bg-mv-primary-3 text-white rounded-full shadow-md hover:bg-mv-secondary-1 transition-all ease-out"
                  disabled={loading}
                >
                  {loading == false ? "Let's Talk" : "Loading"}
                </button>
              </div>
            </form>
          </div>
          <div className="text-gray-700 flex flex-col">
            <div className="">
              <h2 className="text-2xl font-semibold mb-4">Company</h2>
              <div className="grid grid-cols-1 md:grid-cols-2">
                <div className="flex flex-col">
                  <div>
                    <p>
                      <strong>Address:</strong> AD Premier Office Park, Jl. TB
                      Simatupang No.5 17th floor, Suite 04B, RT.5/RW.7, Ragunan,
                      Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota
                      Jakarta 12550
                    </p>
                    <p>
                      <strong>Phone:</strong> {config.phoneNumber}
                    </p>
                    <p>
                      <strong>Email:</strong> {config.email}
                    </p>
                  </div>
                  <div className="mt-3">
                    <iframe
                      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.753622427344!2d106.82327517589971!3d-6.296073761623477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3b69208f7bd%3A0x13f7eb9520cd483e!2sAD%20Premier!5e0!3m2!1sid!2sid!4v1705814261229!5m2!1sid!2sid"
                      width="300"
                      height="300"
                    ></iframe>
                  </div>
                </div>
                <div className="flex flex-col">
                  <div className="w-3/12 lg:w-full mt-5 lg:mt-0 mx-auto">
                    <Image
                      src={logo}
                      alt="salaam inovasi tech"
                      className="h-full w-full"
                    />
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <HomeFooter />
    </>
  );
};

export default Index;
