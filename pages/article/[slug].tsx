import Navbar from "@/components/organisms/Navbar";
import BoxedLayout from "@/components/templates/BoxedLayout";
import Layout from "@/components/templates/Layout";
import Image from "next/image";
import React from "react";
import BreadChumbs from "@/components/molecules/Breadcrumbs";
import placeholder from "@/public/images/thumbnail.jpg";
import articlebanner from "@/public/images/articlebanner.jpg";
import ButtonPrimary from "@/components/atoms/ButtonPrimary";
import HomeFooter from "@/components/templates/HomePage/HomeFooter";
import Seo from "@/components/templates/Seo";
import { api } from "@/lib/graphql/api";
import { ARTICLE, CONFIG } from "@/lib/graphql/query";
import { formatDateHalf } from "@/lib/utils/formatDate";

export type Category = {
  name: string;
  slug?: string;
};

export type Article = {
  title: string;
  slug: string;
  articleCategory: Category;
  thumbnail: any;
  content: any;
  createdAt: string;
};

interface SinglePost {
  article: Article;
}

export async function getServerSideProps({ params }: any) {
  const slug: string = params.slug;
  const { article }: any = await api.request(ARTICLE, { slug });
  const { config }: any = await api.request(CONFIG);

  return {
    props: {
      article,
    },
  };
}

const replaceNewLinesWithBreaks = (text: string) => {
  // Replace <p> tags with empty strings
  let cleanedText = text.replace(/<p>/g, "");

  // Replace </p> tags with <br />
  cleanedText = cleanedText.replace(/<\/p>/g, "<br />");

  // Replace remaining newline characters (\n) with <br />
  cleanedText = cleanedText.replace(/\n/g, "<br />");

  return cleanedText;
};

const slug = ({ article, config }: any) => {
  const cleanedHTML = replaceNewLinesWithBreaks(article.content.html);
  return (
    <div className="w-full flex flex-col">
      <Seo
        metaTitle={article.title}
        metaDesc={article.title}
        metaKey={article.articleCategory.name}
      />

      <Layout>
        <Navbar logo={config.logoHeader.url} />
      </Layout>

      <BoxedLayout>
        <BreadChumbs
          category={`News/${article.articleCategory.name}`}
          title={article.title}
          urlBefore={"/article"}
          urlCurrent={`${article.slug}`}
        />
      </BoxedLayout>

      <BoxedLayout>
        <div className="flex flex-col items-center mt-32 gap-4">
          <p className="font-QuicksandBold text-white text-[18px] px-10 py-2 rounded-xl bg-[#171B26]">
            {article.articleCategory.name}
          </p>
          <h1 className=" font-QuicksandBold text-[32px] leading-[42px] text-center">
            {article.title}
          </h1>
          <p className="font-QuicksandMedium text-[18px] text-center w-6/12">
            {formatDateHalf(article.createdAt)}
          </p>
        </div>
      </BoxedLayout>
      <BoxedLayout>
        <div className="flex flex-col items-center">
          <div className="flex flex-row items-center justify-center flex-wrap gap-6 mt-10 pb-10">
            <Image
              className="lg:p-6 p-2 lg:w-8/12 w-full aspect-square"
              src={article.thumbnail ? article.thumbnail.url : placeholder}
              width={1000}
              height={10}
              alt=""
            />
          </div>
          <div className="mt-5 pb-10">
            <span
              className="font-QuicksandSemibold text-[14px] text-justify leading-[18px]"
              dangerouslySetInnerHTML={{
                __html: cleanedHTML,
              }}
            ></span>
          </div>
        </div>
      </BoxedLayout>
      <HomeFooter />
    </div>
  );
};

export default slug;
