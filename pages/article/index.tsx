import Navbar from "@/components/organisms/Navbar";
import BoxedLayout from "@/components/templates/BoxedLayout";
import Layout from "@/components/templates/Layout";
import Seo from "@/components/templates/Seo";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import placeholder from "@/public/images/trade-learn.png";
import ArticleCard from "@/components/molecules/ArticleCard";
import { useRouter } from "next/router";
import HomeFooter from "@/components/templates/HomePage/HomeFooter";
import { BiSearch } from "react-icons/bi";
import { api } from "@/lib/graphql/api";
import { ARTICLECATEGORY, ARTICLES, CONFIG } from "@/lib/graphql/query";
import { formatDateHalf } from "@/lib/utils/formatDate";
import Paragraph from "@/components/atoms/Paragraph";

type Props = {};

export const getServerSideProps = async () => {
  const { articles }: any = await api.request(ARTICLES);
  const { articleCategories }: any = await api.request(ARTICLECATEGORY);
  const { config }: any = await api.request(CONFIG);

  return {
    props: {
      articles,
      articleCategories,
      config,
    },
  };
};

const Index = ({ articles, articleCategories, config }: any) => {
  const [isClient, setIsClient] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState<string>("All");
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [searchQuery, setSearchQuery] = useState("");
  const sizePerPage = 12;

  const router = useRouter();
  const click = (slug: any) => {
    router.push(`/article/${slug}`);
  };

  const filteredArticles =
    selectedCategory === "All"
      ? articles
      : articles.filter(
          (article: any) => article.articleCategory.name === selectedCategory
        );

  const searchFilteredArticles = filteredArticles.filter((article: any) =>
    article.title.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const totalPages = Math.ceil(filteredArticles.length / sizePerPage);
  const handleCategoryClick = (categoryName: string) => {
    setSelectedCategory(categoryName);
    setCurrentPage(1);
  };

  const handlePreviousPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 1));
  };

  const handleNextPage = () => {
    setCurrentPage((prevPage) => Math.min(prevPage + 1, totalPages));
  };

  const startIndex = (currentPage - 1) * sizePerPage;
  const endIndex = Math.min(startIndex + sizePerPage, filteredArticles.length);

  const handleSearchInput = (query: string) => {
    setSearchQuery(query);
    setCurrentPage(1);
  };

  useEffect(() => {
    setIsClient(true);
  }, []);
  return (
    <div className="w-full">
      <Seo metaTitle="News" />

      <Navbar logo={config.logoHeader.url} />

      <BoxedLayout>
        <div className="flex flex-col items-center lg:mt-32 mt-10 gap-4">
          <h1 className="font-QuicksandBold text-center text-[44px] leading-[44px]">
            Whats <span className="text-mv-primary-1">Happening</span>
          </h1>
          <div className="text-center lg:w-6/12 w-full">
            <Paragraph
              text="Stay ahead of the game by staying tuned for our frequent updates,
            where you can catch up on all the latest happenings, insights, and
            stories from our company."
            />
          </div>
        </div>
      </BoxedLayout>
      <BoxedLayout>
        {config.hideNews == true ? (
          <div className="w-full h-[70vh] flex justify-center items-center">
            <h1 className="font-QuicksandBold text-center text-[44px] leading-[44px]">
              Coming <span className="text-mv-secondary-1">Soon</span> ...
            </h1>
          </div>
        ) : (
          <>
            <div className="w-full mt-10 mb-2 flex lg:flex-row flex-col-reverse items-center">
              <div className="lg:w-1/2 w-full flex flex-wrap gap-2">
                <div
                  onClick={() => handleCategoryClick("All")} // Click handler for "All" category
                  className={`border rounded-[30px] px-4 py-2 cursor-pointer hover:bg-mv-secondary-1 transition-all ease-out hover:text-white ${
                    selectedCategory === "All"
                      ? "bg-mv-secondary-1 text-white"
                      : ""
                  }`}
                >
                  All
                </div>
                {articleCategories.map((category: any, i: any) => (
                  <div
                    key={i}
                    onClick={() => handleCategoryClick(category.name)} // Click handler for individual category
                    className={`border rounded-[30px] px-4 py-2 cursor-pointer hover:bg-mv-secondary-1 transition-all ease-out hover:text-white ${
                      selectedCategory === category.name
                        ? "bg-mv-secondary-1 text-white"
                        : ""
                    }`}
                  >
                    {category.name}
                  </div>
                ))}
              </div>
              <div className="lg:w-1/2 w-full">
                <label
                  htmlFor="eventSearch"
                  className="relative text-gray-400 focus-within:text-gray-600 block"
                >
                  <BiSearch className="pointer-events-none w-5 h-5 absolute top-1/2 transform -translate-y-1/2 right-4 text-mv-primary-1" />
                  <input
                    className="w-full px-5 my-3 py-2 border border-[#E8E8E8] rounded-[30px] focus:outline-none focus:border-mv-primary-1 focus:border-2"
                    type="text"
                    name="eventSearch"
                    id="eventSearch"
                    placeholder="Search Article"
                    value={searchQuery}
                    onChange={(e) => handleSearchInput(e.target.value)}
                  />
                </label>
              </div>
            </div>

            <div className=" lg:grid-cols-4 grid-cols-1 gap-5 mb-20 grid">
              {searchFilteredArticles.length > 0 ? (
                searchFilteredArticles
                  .slice(startIndex, endIndex)
                  .map((v: any, i: number) => {
                    return (
                      <div key={i}>
                        <ArticleCard
                          click={() => {
                            click(v.slug);
                          }}
                          name={v.title}
                          url={v.thumbnail ? v.thumbnail.url : placeholder}
                          tag={v.articleCategory.name}
                          content={v.content.text.slice(0, 150)}
                          date={
                            isClient
                              ? formatDateHalf(v.createdAt)
                              : "Fetch Failed"
                          }
                          slug={v.slug}
                        />
                      </div>
                    );
                  })
              ) : (
                <div className="flex justify-center items-center">
                  No Article Found
                </div>
              )}
            </div>
          </>
        )}

        {/* all articles section end */}

        {/* Pagination */}
        {filteredArticles.length > sizePerPage && (
          <div className="flex justify-center my-5">
            <button
              onClick={handlePreviousPage}
              className="bg-mv-secondary-5 hover:bg-mv-secondary-3 text-white font-bold py-2 px-4 rounded-l"
              disabled={currentPage === 1}
            >
              Previous
            </button>
            <button
              onClick={handleNextPage}
              className="bg-mv-secondary-5 hover:bg-mv-secondary-3 text-white font-bold py-2 px-4 rounded-r"
              disabled={currentPage === totalPages}
            >
              Next
            </button>
          </div>
        )}
        {/* Pagination end */}
      </BoxedLayout>
      <HomeFooter />
    </div>
  );
};

export default Index;
