import AboutUsTemplate from "@/components/templates/AboutUsTemplate";
import Seo from "@/components/templates/Seo";
import { api } from "@/lib/graphql/api";
import { CONFIG } from "@/lib/graphql/query";
import React from "react";

type Props = {};

export const getServerSideProps = async () => {
  const { config }: any = await api.request(CONFIG);

  return {
    props: {
      config,
    },
  };
};

const index = ({ config }: any) => {
  return (
    <>
      <Seo />
      <AboutUsTemplate logo={config.logoHeader.url} />;
    </>
  );
};

export default index;
