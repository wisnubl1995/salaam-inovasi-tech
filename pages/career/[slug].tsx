import Navbar from "@/components/organisms/Navbar";
import BoxedLayout from "@/components/templates/BoxedLayout";
import HomeFooter from "@/components/templates/HomePage/HomeFooter";
import Layout from "@/components/templates/Layout";
import Seo from "@/components/templates/Seo";
import Image from "next/image";
import logo from "@/public/images/aboutusmanwithlaptop.png";
import React, { useState } from "react";
import { api } from "@/lib/graphql/api";
import { CONFIG, JOB } from "@/lib/graphql/query";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ReCAPTCHA from "react-google-recaptcha";

type Props = {};

export async function getServerSideProps({ params }: any) {
  const slug: string = params.slug;
  const { job }: any = await api.request(JOB, { slug });
  const { config }: any = await api.request(CONFIG);

  return {
    props: {
      job,
      config,
    },
  };
}

const Slug = ({ job, config }: any) => {
  const [cvFile, setCvFile] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [recaptchaValue, setRecaptchaValue] = useState<string | null>(null);

  const handleFileChange = (event: any) => {
    setCvFile(event.target.files[0]);
  };

  const handleApplyClick = () => {
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <Seo />

      <Navbar logo={config.logoHeader.url} />

      <BoxedLayout>
        <div className=" w-full py-10">
          <div className="flex lg:flex-row flex-col justify-between w-full">
            <div className="lg:w-6/12 w-full px-5 py-2">
              <div className="w-full flex h-full object-cover">
                <Image
                  src={logo}
                  alt="logo"
                  width={1000}
                  height={0}
                  className="w-full object-contain"
                />
              </div>
            </div>
            <div className="lg:w-6/12 w-full px-5 py-2 flex flex-col gap-3 justify-center items-start">
              <div>
                <h3 className="font-QuicksandSemibold text-[24px] leading-none cursor-pointer">
                  {job.jobRole}
                </h3>
                <div className="flex flex-row gap-2">
                  <h3 className="font-QuicksandRegular text-[10px]">
                    {job.jobLevel}
                  </h3>
                  <h3>-</h3>
                  <h3 className="font-QuicksandRegular text-[10px]">
                    {job.fullTime == true ? "Full-time" : "Contract"}
                  </h3>
                  <h3>-</h3>
                  <h3 className="font-QuicksandRegular text-[10px]">
                    {job.workingSite}
                  </h3>
                </div>
              </div>
              <div>
                <div>
                  <h3 className="font-QuicksandBold text-[14px]">
                    Main Responsibility
                  </h3>
                </div>
                <div
                  className="text-[10px] font-QuicksandSemibold "
                  dangerouslySetInnerHTML={{
                    __html: job.mainResponsibility.html,
                  }}
                />
              </div>
              <div className="flex lg:flex-row flex-col justify-between w-full">
                <div className="lg:w-6/12 w-full">
                  <div>
                    <h3 className="font-QuicksandBold text-[14px]">
                      Minimum Requirements
                    </h3>
                  </div>
                  <div
                    className="text-[10px] font-QuicksandSemibold "
                    dangerouslySetInnerHTML={{
                      __html: job.requirements.html,
                    }}
                  />
                </div>
                <div className="lg:w-6/12 w-full">
                  <div>
                    <h3 className="font-QuicksandBold text-[14px]">Benefits</h3>
                  </div>
                  <div
                    className="text-[10px] font-QuicksandSemibold "
                    dangerouslySetInnerHTML={{
                      __html: job.benefits.html,
                    }}
                  />
                </div>
              </div>
              <button
                onClick={handleApplyClick}
                className="text-[10px] px-7 py-[1px] bg-mv-primary-1 hover:bg-mv-secondary-1 text-white transition-all ease-out duration-200 rounded-xl font-QuicksandBold"
              >
                Apply
              </button>
              <h3 className="font-QuicksandRegular text-[10px]">
                Or, you can send your updated CV to
                <a href="mailto:hr@salaaminovasi.tech">
                  <strong> hr@salaaminovasi.tech </strong>
                </a>
                manually
              </h3>
            </div>
          </div>
        </div>
      </BoxedLayout>
      <HomeFooter />

      {isModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
          <div className="bg-white p-8 rounded-lg relative">
            <button
              onClick={handleCloseModal}
              className="text-[10px] absolute top-3 right-2 px-7 py-[1px] bg-mv-primary-1 hover:bg-mv-secondary-1 text-white transition-all ease-out duration-200 rounded-xl font-QuicksandBold"
            >
              close
            </button>
            <p>Please fill the data below</p>

            <form className="mx-auto  max-w-xl ">
              <div className="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">
                <div>
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    First name
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="text"
                      name="firstName"
                      id="firstName"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div>
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Last name
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="text"
                      name="lastName"
                      id="lastName"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>

                <div className="sm:col-span-2">
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Email
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="email"
                      name="email"
                      id="email"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div className="sm:col-span-2">
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Phone Number
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="text"
                      placeholder="08XX XXXX XXXX"
                      name="phoneNumber"
                      id="phoneNumber"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div className="sm:col-span-2">
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Expected Salary
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="text"
                      placeholder="Please fill in IDR"
                      name="salary"
                      id="salary"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>

                <div className="flex gap-x-4 sm:col-span-2"></div>
              </div>
              <div className="mt-2">
                <ReCAPTCHA
                  sitekey="YOUR_RECAPTCHA_SITE_KEY"
                  onChange={(value) => setRecaptchaValue(value)}
                />
              </div>
              <div>
                <h3 className="font-QuicksandBold text-[14px]">Upload CV</h3>
                <input
                  type="file"
                  required
                  accept=".pdf"
                  onChange={handleFileChange}
                />
                <button
                  type="submit"
                  className="text-[10px] px-7 py-3 bg-mv-primary-1 hover:bg-mv-secondary-1 text-white transition-all ease-out duration-200 rounded-xl font-QuicksandBold"
                >
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </>
  );
};

export default Slug;
