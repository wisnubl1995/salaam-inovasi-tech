import Seo from "@/components/templates/Seo";
import React, { useState, useEffect } from "react";
import Navbar from "@/components/organisms/Navbar";
import { BiSearch } from "react-icons/bi";
import HomePageSubHeader from "@/components/molecules/HomePageSubHeader";
import Subtitle from "@/components/atoms/Subtitle";
import Layout from "@/components/templates/Layout";
import HomeFooter from "@/components/templates/HomePage/HomeFooter";
import BoxedLayout from "@/components/templates/BoxedLayout";
import { api } from "@/lib/graphql/api";
import { CONFIG, JOBS } from "@/lib/graphql/query";
import Image from "next/image";
import logo from "@/public/logo.png";
import { useRouter } from "next/router";
import Link from "next/link";

type Props = {};

export const getServerSideProps = async () => {
  const { jobs }: any = await api.request(JOBS);
  const { config }: any = await api.request(CONFIG);
  return {
    props: {
      jobs,
      config,
    },
  };
};

const Events = ({ jobs, config }: any) => {
  const router = useRouter();
  const click = (slug: any) => {
    router.push(`/career/${slug}`);
  };

  return (
    <>
      <div className="">
        <Seo />
        <div>
          <Navbar logo={config.logoHeader.url} />

          <BoxedLayout>
            <div className="flex flex-col text-center gap-8 justify-center w-1/2 mx-auto mt-20 mb-22">
              <HomePageSubHeader textColor="Join" text="Our Forces!" />
              <Subtitle text="Stay ahead of the game by staying tuned for our frequent updates, where you can catch up on all the latest jobs from our company." />
            </div>

            <div className="">
              <div className="w-full mt-10 mb-2 flex lg:flex-row flex-col-reverse items-center">
                <div className="lg:w-1/2 w-full flex flex-wrap gap-2"></div>
                <div className="lg:w-1/2 w-full">
                  <label
                    htmlFor="eventSearch"
                    className="relative text-gray-400 focus-within:text-gray-600 block"
                  >
                    <BiSearch className="pointer-events-none w-5 h-5 absolute top-1/2 transform -translate-y-1/2 right-4 text-mv-primary-1" />
                    <input
                      className="w-full px-5 my-3 py-2 border border-[#E8E8E8] rounded-[30px] focus:outline-none focus:border-mv-primary-1 focus:border-2"
                      type="text"
                      name="eventSearch"
                      id="eventSearch"
                      placeholder="Search Jobs"
                    />
                  </label>
                </div>
              </div>
            </div>

            {config.hideCareer == true ? (
              <div className="w-full h-[70vh] flex justify-center items-center">
                <h1 className="font-QuicksandBold text-center text-[44px] leading-[44px]">
                  Coming <span className="text-mv-primary-1">Soon</span> ...
                </h1>
              </div>
            ) : (
              <div className="w-full py-2">
                <div className="flex lg:flex-row flex-col lg:flex-wrap justify-center px-3 items-center w-full  gap-3 pb-20">
                  {jobs.length > 0
                    ? jobs.map((job: any, i: any) => {
                        return (
                          <div
                            key={i}
                            className="bg-white gap-3 lg:w-3/12 w-full lg:h-[250px] h-full py-5 lg:py-[2px] rounded-2xl shadow-lg hover:shadow-2xl transition-all ease-out duration-200 px-5 flex flex-col justify-start items-start"
                          >
                            <div className="w-full  flex justify-center items-center">
                              <Image className="" src={logo} alt="logo" />
                            </div>
                            <div className="w-full">
                              <div>
                                <Link href={`/career/${job.slug}`}>
                                  <h3 className="font-QuicksandSemibold text-[16px] leading-none cursor-pointer">
                                    {job.jobRole}
                                  </h3>
                                </Link>
                              </div>
                              <div className="flex flex-row justify-between ">
                                <div className="flex flex-col">
                                  <h3 className="font-QuicksandRegular text-[10px]">
                                    {job.fullTime == true
                                      ? "Full-time"
                                      : "Contract"}
                                  </h3>
                                  <h3 className="font-QuicksandBold text-[10px]">
                                    {job.workingSite}
                                  </h3>
                                </div>
                                <div className="flex items-end">
                                  <button
                                    onClick={() => {
                                      click(job.slug);
                                    }}
                                    className="text-[10px] px-5 py-[1px] bg-mv-primary-1 text-white font-QuicksandBold rounded-2xl hover:bg-mv-secondary-1 transition-all ease-out duration-200"
                                  >
                                    Detail
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })
                    : "Sorry, We Have No Job Vacancies At This Moment.."}
                </div>
              </div>
            )}
          </BoxedLayout>

          <HomeFooter />
        </div>
      </div>
    </>
  );
};

export default Events;
