import type { NextApiRequest, NextApiResponse } from "next";
import formidable from "formidable";
import sendEmail from "@/lib/utils/sendMail";
import fs from "fs";

const handler = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  try {
    const form = new formidable.IncomingForm({
      uploadDir: "uploads/",
    });

    form.parse(req, async (err: any, fields: any, files: any) => {
      if (err) {
        return res.status(500).json({ error: err.message });
      }

      const cvPath = files.cv.path as string;
      const emailOptions = {
        to: "akunbaruwisnu@gmail.com", // Replace with the recipient's email
        subject: "New CV Submission",
        text: "A new CV has been submitted. Please find the attached CV file.",
        attachments: [{ filename: "cv.pdf", content: fs.readFileSync(cvPath) }],
      };

      sendEmail(emailOptions);

      res.status(200).json({ message: "CV submitted successfully" });
    });
  } catch (error) {
    console.error("Error processing CV submission:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

export const config = {
  api: {
    bodyParser: false,
  },
};

export default handler;
