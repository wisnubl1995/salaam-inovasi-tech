import Navbar from "@/components/organisms/Navbar";
import BoxedLayout from "@/components/templates/BoxedLayout";
import Layout from "@/components/templates/Layout";
import Seo from "@/components/templates/Seo";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import placeholder from "@/public/images/trade-learn.png";

import { useRouter } from "next/router";
import HomeFooter from "@/components/templates/HomePage/HomeFooter";
import { BiSearch } from "react-icons/bi";
import { api } from "@/lib/graphql/api";
import { CONFIG, PROJECTCATEGORIES, PROJECTS } from "@/lib/graphql/query";
import { formatDateHalf } from "@/lib/utils/formatDate";
import Paragraph from "@/components/atoms/Paragraph";
import ArticleCard from "@/components/molecules/ArticleCard";
import ButtonPrimary from "@/components/atoms/ButtonPrimary";

type Props = {};

export const getServerSideProps = async () => {
  const { projects }: any = await api.request(PROJECTS);
  const { projectCategories }: any = await api.request(PROJECTCATEGORIES);
  const { config }: any = await api.request(CONFIG);

  return {
    props: {
      projects,
      projectCategories,
      config,
    },
  };
};

const Index = ({ projects, projectCategories, config }: any) => {
  const [isClient, setIsClient] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState<string>("All");
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [searchQuery, setSearchQuery] = useState("");
  const sizePerPage = 12;

  const router = useRouter();
  const click = (slug: any) => {
    router.push(`/expertise/${slug}`);
  };

  const filteredprojects =
    selectedCategory === "All"
      ? projects
      : projects.filter(
          (project: any) =>
            project.projectCategory.categoryName === selectedCategory
        );

  const searchFilteredprojects = filteredprojects.filter((project: any) =>
    project.projectName.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const totalPages = Math.ceil(filteredprojects.length / sizePerPage);
  const handleCategoryClick = (categoryName: string) => {
    setSelectedCategory(categoryName);
    setCurrentPage(1);
  };

  const handlePreviousPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 1));
  };

  const handleNextPage = () => {
    setCurrentPage((prevPage) => Math.min(prevPage + 1, totalPages));
  };

  const startIndex = (currentPage - 1) * sizePerPage;
  const endIndex = Math.min(startIndex + sizePerPage, filteredprojects.length);

  const handleSearchInput = (query: string) => {
    setSearchQuery(query);
    setCurrentPage(1);
  };

  useEffect(() => {
    setIsClient(true);
  }, []);
  return (
    <div className="w-full">
      <Seo metaTitle="Career" />

      <Navbar logo={config.logoHeader.url} />

      <BoxedLayout>
        <div className="flex flex-col items-center lg:mt-32 mt-10 gap-4">
          <h1 className="font-QuicksandBold text-center text-[44px] leading-[44px]">
            Our <span className="text-mv-primary-1">Projects</span>
          </h1>
          <div className="text-center lg:w-6/12 w-full">
            <Paragraph text="Stay ahead of our beautiful projects from our wonderful clients!" />
          </div>
        </div>
      </BoxedLayout>
      <BoxedLayout>
        <div className="w-full mt-10 mb-2 flex lg:flex-row flex-col-reverse items-center">
          <div className="lg:w-1/2 w-full flex flex-wrap gap-2">
            <div
              onClick={() => handleCategoryClick("All")} // Click handler for "All" category
              className={`border rounded-[30px] px-4 py-2 cursor-pointer hover:bg-mv-secondary-1 transition-all ease-out hover:text-white ${
                selectedCategory === "All" ? "bg-mv-secondary-1 text-white" : ""
              }`}
            >
              All
            </div>
            {projectCategories.map((category: any, i: any) => (
              <div
                key={i}
                onClick={() => handleCategoryClick(category.categoryName)} // Click handler for individual category
                className={`border rounded-[30px] px-4 py-2 cursor-pointer hover:bg-mv-secondary-1 transition-all ease-out hover:text-white ${
                  selectedCategory === category.CategoryName
                    ? "bg-mv-secondary-1 text-white"
                    : ""
                }`}
              >
                {category.categoryName}
              </div>
            ))}
          </div>
          <div className="lg:w-1/2 w-full">
            <label
              htmlFor="eventSearch"
              className="relative text-gray-400 focus-within:text-gray-600 block"
            >
              <BiSearch className="pointer-events-none w-5 h-5 absolute top-1/2 transform -translate-y-1/2 right-4 text-mv-primary-1" />
              <input
                className="w-full px-5 my-3 py-2 border border-[#E8E8E8] rounded-[30px] focus:outline-none focus:border-mv-primary-1 focus:border-2"
                type="text"
                name="eventSearch"
                id="eventSearch"
                placeholder="Search project"
                value={searchQuery}
                onChange={(e) => handleSearchInput(e.target.value)}
              />
            </label>
          </div>
        </div>

        <div className="grid lg:grid-cols-4 grid-cols-1 gap-5 mb-20">
          {searchFilteredprojects
            .slice(startIndex, endIndex)
            .map((v: any, i: number) => {
              return (
                <div key={i}>
                  <div className="w-full px-2 py-3 bg-white shadow-xl flex flex-col gap-2 rounded-xl font-QuicksandRegular">
                    <div className="w-full bg-slate-300/30 h-[200px] overflow-hidden flex justify-center items-center relative rounded-2xl">
                      <Image
                        src={v.imageThumbnail.url}
                        alt={v.projectName}
                        width={200}
                        height={0}
                        className="w-full h-full object-contain"
                      />
                      <div className="absolute top-3 left-1">
                        <button className="text-white bg-black px-2 rounded-xl py-[1px] font-QuicksandBold text-[10px] ">
                          {v.projectCategory.categoryName}
                        </button>
                      </div>
                    </div>

                    <div>
                      <h3
                        onClick={() => {
                          click(v.slug);
                        }}
                        className="font-QuicksandSemibold text-[16px] leading-none cursor-pointer"
                      >
                        {v.projectName}
                      </h3>
                      <h3 className="font-QuicksandRegular text-[10px]">
                        {v.client.clientName}
                      </h3>
                    </div>

                    <div>
                      <button
                        onClick={() => {
                          click(v.slug);
                        }}
                        className="text-[12px] px-3 bg-mv-primary-1 hover:bg-mv-secondary-1 text-white rounded-xl transition-all ease-out duration-200"
                      >
                        Detail
                      </button>
                    </div>
                  </div>
                  {/* {<ArticleCard
                    click={() => {
                      click(v.slug);
                    }}
                    name={v.projectName}
                    url={v.thumbnail ? v.thumbnail.url : placeholder}
                    tag={v.projectCategory.categoryName}
                    content={v.projectDescription}
                    date={
                      isClient ? formatDateHalf(v.createdAt) : "Fetch Failed"
                    }
                    slug={v.slug}
                  />} */}
                </div>
              );
            })}
        </div>

        {/* all projects section end */}

        {/* Pagination */}
        {filteredprojects.length > sizePerPage && (
          <div className="flex justify-center my-5">
            <button
              onClick={handlePreviousPage}
              className="bg-mv-secondary-5 hover:bg-mv-secondary-3 text-white font-bold py-2 px-4 rounded-l"
              disabled={currentPage === 1}
            >
              Previous
            </button>
            <button
              onClick={handleNextPage}
              className="bg-mv-secondary-5 hover:bg-mv-secondary-3 text-white font-bold py-2 px-4 rounded-r"
              disabled={currentPage === totalPages}
            >
              Next
            </button>
          </div>
        )}
        {/* Pagination end */}
      </BoxedLayout>
      <HomeFooter />
    </div>
  );
};

export default Index;
