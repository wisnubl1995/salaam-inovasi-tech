import Navbar from "@/components/organisms/Navbar";
import BoxedLayout from "@/components/templates/BoxedLayout";
import Layout from "@/components/templates/Layout";
import Image from "next/image";
import React from "react";
import BreadChumbs from "@/components/molecules/Breadcrumbs";
import placeholder from "@/public/images/thumbnail.jpg";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import HomeFooter from "@/components/templates/HomePage/HomeFooter";
import Seo from "@/components/templates/Seo";
import { api } from "@/lib/graphql/api";
import { ARTICLE, CONFIG, PROJECT } from "@/lib/graphql/query";
import { formatDateHalf } from "@/lib/utils/formatDate";

export async function getServerSideProps({ params }: any) {
  const slug: string = params.slug;
  const { project }: any = await api.request(PROJECT, { slug });
  const { config }: any = await api.request(CONFIG);

  return {
    props: {
      project,
      config,
    },
  };
}

const slug = ({ project, config }: any) => {
  return (
    <div className="w-full flex flex-col">
      <Seo
        metaTitle={project.projectName}
        metaDesc={project.projectName}
        metaKey={project.projectCategory.categoryName}
      />

      <Navbar logo={config.logoHeader.url} />

      <BoxedLayout>
        <div className="flex flex-col items-center mt-32 gap-4">
          <p className="font-QuicksandBold text-white text-[18px] px-10 py-2 rounded-xl bg-[#171B26]">
            {project.projectCategory.categoryName}
          </p>
          <h1 className=" font-QuicksandBold text-[32px] leading-[42px] text-center">
            {project.projectName}
          </h1>
          <p className="font-QuicksandMedium text-[18px] text-center w-6/12">
            {formatDateHalf(project.createdAt)}
          </p>
        </div>
      </BoxedLayout>

      <div className="flex flex-col items-center w-full mx-auto">
        <Slider
          className="w-10/12 lg:w-8/12 "
          slidesToShow={1}
          slidesToScroll={1}
          infinite={true}
          autoplay={true}
          autoplaySpeed={1500}
          dots={true}
        >
          {project.additionalImage.map((image: any, index: number) => (
            <Image
              key={index}
              className={` ${
                project.projectCategory.categoryName === "Mobile Application"
                  ? "lg:w-[100px] lg:h-[350px]  w-full object-contain"
                  : "aspect-video  w-full"
              }`}
              src={image.url}
              width={1000}
              height={10}
              alt=""
            />
          ))}
        </Slider>

        <BoxedLayout>
          <div className="mt-24 pb-10 lg:text-left text-center">
            <span className="font-QuicksandSemibold text-[14px] text-justify leading-[18px]">
              {project.projectDescription}
            </span>
          </div>
        </BoxedLayout>
      </div>

      <HomeFooter />
    </div>
  );
};

export default slug;
