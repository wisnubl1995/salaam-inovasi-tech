/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    fontFamily: {
      QuicksandLight: ["QuicksandLight"],
      QuicksandRegular: ["QuicksandRegular"],
      QuicksandMedium: ["QuicksandMedium"],
      QuicksandSemibold: ["QuicksandSemibold"],
      QuicksandBold: ["QuicksandBold"],
      RobotoLight: ["RobotoLight"],
      RobotoRegular: ["RobotoRegular"],
      RobotoMedium: ["RobotoMedium"],
      RobotoSemibold: ["RobotoSemibold"],
      RobotoBold: ["RobotoBold"],
    },
    extend: {
      maxWidth: {
        main: "1440px",
      },
      colors: {
        "mv-primary": {
          1: "#3E449B",
          2: "#3E449B",
          3: "#3E449B",
          4: "#3E449B",
          5: "#3E449B",
          6: "#3E449B",
        },
        "mv-secondary": {
          1: "#6FCBDC",
          6: "#6FCBDC",
          5: "#6FCBDC",
          4: "#6FCBDC",
          3: "#6FCBDC",
          2: "#6FCBDC",
        },
        "mv-gray": {
          1: "#1C1C1C",
          2: "#666666",
          3: "#8A8A8A",
          4: "#505050",
          5: "#383838",
          6: "#000000",
        },
        "mv-white": {
          1: "#F2F2F2",
          2: "#FCFCFC",
          3: "#F4F5F5",
          4: "#F4F7FF",
          5: "#e2e2e2",
          6: "#D9D9D9",
          7: "#BFBFBF",
        },
      },
      boxShadow: {
        gradient: "-15px -5px 100px -10px #3E449B, 5px 15px 70px -20px #6FCBDC",
        blur: "0px 0px 10000px 100px #f1f1f1",
      },
      backgroundImage: {
        hero: "url('/images/herobg.png')",
        arrow: "url('/images/arrow.png')",
        auth: "url('/images/authbanner.jpg')",
        aboutusheader: "url('/images/aboutusheaderbg.png')",
        aboutusfooter: "url('/images/aboutusfooterbg.png')",
        contactus: "url('/images/contactus.png')",
        eventMark: "url('/images/eventMark.png')",
        maintenance: "url('/images/maintenanceWave.png')",
      },
    },
  },
  plugins: [],
};
