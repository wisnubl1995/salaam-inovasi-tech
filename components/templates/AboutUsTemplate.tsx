import React from "react";
import OneLiner from "@/components/atoms/OneLiner";
import Navbar from "@/components/organisms/Navbar";
import Layout from "@/components/templates/Layout";
import Logo from "@/public/logo.png";
import ManWithLaptop from "/public/images/aboutusmanwithlaptop.png";
import Happyman from "@/public/stand.svg";
import Image from "next/image";
import AboutUsCard from "@/components/molecules/AboutUsCard";
import Screen6 from "@/components/templates/HomePage/Screen6";
import lampu from "@/public/icon/lampu.svg";
import connect from "@/public/icon/connect.svg";
import grow from "@/public/icon/grow.svg";
import AboutUsCardFooter from "@/components/molecules/AboutUsCardFooter";
import Paragraph from "@/components/atoms/Paragraph";
import HomeFooter from "./HomePage/HomeFooter";
import vision from "@/public/vision.svg";
import mission from "@/public/mission.svg";

type Props = {};

const AboutUsTemplate = ({ logo }: any) => {
  return (
    <div className="w-full">
      <div className="relative bg-aboutusheader bg-cover bg-center rounded-b-[75px] h-full  bg-no-repeat mb-12">
        <Layout>
          <Navbar
            logo={logo}
            className={"bg-white/90 rounded-br-xl rounded-bl-xl "}
          />
          <div className="flex flex-col items-center justify-center h-96 gap-8 relative z-50 text-white">
            <OneLiner text="The beauty of connection" />
            <Paragraph text="Learn how we bring connection to the world!" />
          </div>
        </Layout>
      </div>
      <div className="flex flex-col items-center px-3 justify-center gap-2 md:px-[72px] lg:px-[72px]">
        <div className="w-[60px]">
          <Image src={Logo} alt="" width={100} height={10} />
        </div>
        <div className="flex items-center ">
          <div className="text-[48px] font-QuicksandBold tracking-normal leading-[48px]">
            About <span className="text-mv-secondary-4">Us</span>
          </div>
        </div>
        <div className="flex">
          <div className="w-1/2">
            <AboutUsCard
              title="Software Solution"
              text={
                "As the flagship of Salaam Innovation Technology, we've been pioneering in the tech realm since 2023. We specialize in crafting web and mobile-based solutions for internal company management systems. Offering versatile software platforms, seamlessly integrating with both new and existing client applications."
              }
            />
          </div>
          <div className="w-1/2 h-96 flex items-center justify-center">
            <Image
              src={ManWithLaptop}
              alt=""
              width={400}
              height={400}
              className="w-3/4 h-auto"
            />
          </div>
        </div>
        <div className="flex">
          <div className="w-1/2">
            <Image
              src={vision}
              alt=""
              width={400}
              height={400}
              className="w-3/4 h-auto"
            />
          </div>
          <div className="w-1/2">
            <AboutUsCard
              rightJustify
              title="Vision"
              text={
                "Transforming into a global player, Salaam Innovation Technology emerges as a powerhouse in information and communication technology. We contribute significantly to businesses' digital journey, providing impactful solutions in the digital realm."
              }
            />
          </div>
        </div>
        <div className="flex">
          <div className="w-1/2">
            <AboutUsCard
              title="Mission"
              text="
Prioritizing professionalism, individual expertise, and teamwork to deliver high-quality applications. Offering optimal solutions to partners, addressing various challenges in the field of information technology."
            />
          </div>
          <div className="w-1/2 lg:-mt-72 flex justify-center">
            <Image
              src={mission}
              alt=""
              width={400}
              height={400}
              className="w-3/4 h-auto"
            />
          </div>
        </div>
      </div>
      <div className="bg-contain bg-bottom h-full w-full mt-14 relative bg-no-repeat flex">
        <div className="w-4/12 h-full mt-16 absolute left-[5%] hidden lg:block">
          <Image src={Happyman} alt="" priority className="w-full h-auto" />
        </div>
        <div className="flex lg:justify-end justify-center  w-full pb-64 pr-8">
          <div className="w-3/5 h-3/4 lg:mt-40 mt-10 flex flex-col justify-center">
            <div className="lg:text-[64px] text-[20px] font-QuicksandBold tracking-normal lg:leading-[48px] text-black text-center my-8">
              What we stand for
            </div>
            <div className="flex lg:flex-row flex-col justify-center items-center text-center lg:text-left gap-2">
              <AboutUsCardFooter
                image={lampu}
                title="Learn."
                paragraph="Ignite curiosity with our commitment to continuous learning. We empower individuals and teams through knowledge acquisition, fostering a culture of innovation and expertise."
              />
              <AboutUsCardFooter
                image={connect}
                title="Connect."
                paragraph="Forge meaningful connections in a digital era. We prioritize open communication, collaboration, and networking, creating a supportive environment that values relationships and shared success."
              />
              <AboutUsCardFooter
                image={grow}
                title="Grow."
                paragraph="Cultivate growth, both personal and professional. Our focus extends beyond milestones, nurturing development, and adaptability. Join us in a journey of continuous improvement and sustainable success."
              />
            </div>
          </div>
        </div>
      </div>
      <HomeFooter />
    </div>
  );
};

export default AboutUsTemplate;
