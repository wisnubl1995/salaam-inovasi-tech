import React, { ReactNode } from "react";

type Props = {
  children: ReactNode;
};
const BoxedLayout = ({ children }: Props) => {
  return (
    <div className="text-base font-RobotoRegular tracking-normal mx-auto max-w-[1360px] lg:px-32 px-6">
      {children}
    </div>
  );
};

export default BoxedLayout;
