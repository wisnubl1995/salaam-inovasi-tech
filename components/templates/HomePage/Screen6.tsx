import ButtonWhite from "@/components/atoms/ButtonWhite";
import HeavenCloud from "/public/havencloud.svg";
import HeavenFoot from "/public/havenfoot.png";
import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import bg from "@/public/connect.svg";

type Props = {
  click?: any;
};

const Screen6 = (props: Props) => {
  const [loading, setLoading] = useState(false);
  const contactHandler = async (event: any) => {
    event.preventDefault();
    const target = event.target;
    const { email, firstName, lastName, phoneNumber, company, message } =
      target;
    try {
      setLoading(true);
      const response = await fetch("/api/email", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email.value,
          firstName: firstName.value,
          lastName: lastName.value,
          phoneNumber: phoneNumber.value,
          company: company.value,
          message: message.value,
        }),
      });
      if (response.ok) {
        alert("Your message has been sent.");

        email.value = "";
        setLoading(false);
      } else {
        alert("Failed to send your message. Please try again.");
        setLoading(false);
      }
    } catch (error) {
      alert("INTERNAL ERROR");
    }
  };
  return (
    <section className=" text-black font-QuicksandMedium py-16 px-24">
      <div className="container mx-auto flex lg:flex-row flex-col items-center">
        {/* Left Column - Image */}
        <div className="w-full lg:w-1/2">
          <Image
            src={bg}
            alt="Let's Grow with Us"
            className="w-full h-full object-cover"
          />
        </div>

        {/* Right Column - Wording and CTA */}
        <div className="w-full lg:w-1/2  lg:px-8 lg:text-left text-center">
          <h1 className="text-4xl font-bold mb-6">Let&apos;s Grow with Us</h1>
          <p className="text-lg mb-8">
            Explore new opportunities and take your business to the next level
            with our expertise. Connect with us today!
          </p>
          <div>
            <Link
              href="/contact-us"
              className="bg-mv-primary-1 text-white px-8 py-3 w-full rounded-full hover:bg-mv-secondary-1 transition-all ease-out"
            >
              Let&apos;s Talk
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Screen6;
