import React, { FormEvent } from "react";
import Image from "next/image";
import Icon from "/public/logo-full.png";
import Container from "@/components/atoms/Container";
import Layout from "../Layout";
import BoxedLayout from "../BoxedLayout";
import Link from "next/link";
import Paragraph from "@/components/atoms/Paragraph";

type Props = {};

interface ContactFormElement extends EventTarget {
  email: {
    value: string;
  };
}

const HomeFooter = (props: Props) => {
  const emailHandler = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const target = event.target as ContactFormElement;
    const { email } = target;

    const response = await fetch("/api/contact", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email.value,
      }),
    });

    if (response.ok) {
      alert("Your message has been sent.");

      email.value = "";
    } else {
      alert("Failed to send your message. Please try again.");
    }
  };
  return (
    <div className="w-full bg-white rounded-t-[30px] shadow-[5px_-5px_25px_-15px_rgba(0,0,0,0.3)] py-10">
      <div className="max-w-[1360px] mx-auto px-1 lg:px-10">
        <div className="mt-10 flex flex-col lg:flex-row pb-10 w-full p-6 lg:p-0">
          <div className="lg:w-6/12 flex flex-col gap-5">
            <figure>
              <Image src={Icon} alt="" width={150} height={150} />
            </figure>
            <div className="lg:w-9/12">
              <Paragraph text="AD Premier Office Park, Jl. TB Simatupang No.5 17th floor, Suite 04B, RT.5/RW.7, Ragunan, Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12550" />
            </div>
          </div>
          <div className="lg:w-9/12 w-full flex flex-col">
            <div className="w-full lg:flex grid grid-cols-2  mt-10 lg:mt-0 lg:flex-row justify-end gap-10 lg:gap-14 ">
              <div className="flex flex-col gap-5">
                <h3 className="font-QuicksandBold text-[18px] leading-[12px] text-mv-primary-1">
                  Company
                </h3>
                <div className="flex flex-col font-QuicksandMedium text-[18px] leading-[12px] gap-6">
                  <Link href={"/aboutus"}>
                    <div className="cursor-pointer hover:text-mv-primary-1 transition-all ease-out">
                      About Us
                    </div>
                  </Link>
                </div>
              </div>

              <div className="flex flex-col gap-5">
                <h3 className="font-QuicksandBold text-[18px] leading-[12px] text-mv-primary-1">
                  Career
                </h3>
                <div className="flex flex-col font-QuicksandMedium text-[18px] leading-[12px] gap-6">
                  <Link href={"/career"}>
                    <div className="cursor-pointer hover:text-mv-primary-1 transition-all ease-out">
                      Professional
                    </div>
                  </Link>
                </div>
              </div>
              <div className="flex flex-col gap-5">
                <h3 className="font-QuicksandBold text-[18px] leading-[12px] text-mv-primary-1">
                  Resources
                </h3>
                <div className="flex flex-col font-QuicksandMedium text-[18px] leading-[12px] gap-6">
                  <Link href={"/article"}>
                    <div className="cursor-pointer hover:text-mv-primary-1 transition-all ease-out">
                      News
                    </div>
                  </Link>
                </div>
              </div>

              <div className="flex flex-col gap-5">
                <h3 className="font-QuicksandBold text-[18px] leading-[20px] lg:leading-[12px] text-mv-primary-1">
                  Follow Our Socials
                </h3>
                <div className="flex flex-row font-QuicksandMedium text-[18px] leading-[12px] gap-3">
                  {/* twitter */}
                  <Link
                    href={
                      "https://www.tiktok.com/@salaaminovasiteknologi?_t=8j7i1N1BoXZ&_r=1"
                    }
                    target="_blank"
                  >
                    <div className="hover:scale-150 transition-all ease-out">
                      <svg
                        fill="#000000"
                        viewBox="0 0 512 512"
                        width="25"
                        height="25"
                        id="icons"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M412.19,118.66a109.27,109.27,0,0,1-9.45-5.5,132.87,132.87,0,0,1-24.27-20.62c-18.1-20.71-24.86-41.72-27.35-56.43h.1C349.14,23.9,350,16,350.13,16H267.69V334.78c0,4.28,0,8.51-.18,12.69,0,.52-.05,1-.08,1.56,0,.23,0,.47-.05.71,0,.06,0,.12,0,.18a70,70,0,0,1-35.22,55.56,68.8,68.8,0,0,1-34.11,9c-38.41,0-69.54-31.32-69.54-70s31.13-70,69.54-70a68.9,68.9,0,0,1,21.41,3.39l.1-83.94a153.14,153.14,0,0,0-118,34.52,161.79,161.79,0,0,0-35.3,43.53c-3.48,6-16.61,30.11-18.2,69.24-1,22.21,5.67,45.22,8.85,54.73v.2c2,5.6,9.75,24.71,22.38,40.82A167.53,167.53,0,0,0,115,470.66v-.2l.2.2C155.11,497.78,199.36,496,199.36,496c7.66-.31,33.32,0,62.46-13.81,32.32-15.31,50.72-38.12,50.72-38.12a158.46,158.46,0,0,0,27.64-45.93c7.46-19.61,9.95-43.13,9.95-52.53V176.49c1,.6,14.32,9.41,14.32,9.41s19.19,12.3,49.13,20.31c21.48,5.7,50.42,6.9,50.42,6.9V131.27C453.86,132.37,433.27,129.17,412.19,118.66Z"
                          fill="#6FCBDC"
                        />
                      </svg>
                    </div>
                  </Link>
                  {/* instagram */}
                  <Link
                    href={
                      "https://www.instagram.com/salaaminovasiteknologi?igsh=dGFjMXVkMGw1bHJ0&utm_source=qr"
                    }
                    target="_blank"
                  >
                    <div className="hover:scale-150 transition-all ease-out">
                      <svg
                        width="27"
                        height="27"
                        viewBox="0 0 27 27"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M22.7471 9.61239C22.7362 8.7919 22.5826 7.97957 22.2932 7.21173C22.0422 6.56406 21.6589 5.97586 21.1678 5.48471C20.6766 4.99356 20.0884 4.61026 19.4408 4.35931C18.6828 4.07478 17.882 3.92093 17.0726 3.90431C16.0304 3.85773 15.7 3.84473 13.0545 3.84473C10.409 3.84473 10.0699 3.84473 9.03535 3.90431C8.22627 3.92105 7.4259 4.0749 6.66827 4.35931C6.0205 4.61009 5.43221 4.99332 4.94104 5.4845C4.44987 5.97567 4.06663 6.56396 3.81585 7.21173C3.53075 7.96912 3.37724 8.76968 3.36194 9.57881C3.31535 10.6221 3.30127 10.9525 3.30127 13.598C3.30127 16.2435 3.30127 16.5815 3.36194 17.6171C3.37819 18.4275 3.53094 19.227 3.81585 19.9864C4.06705 20.634 4.45057 21.222 4.94191 21.713C5.43324 22.2039 6.02159 22.587 6.66935 22.8377C7.42491 23.1337 8.22542 23.2986 9.03644 23.3252C10.0797 23.3718 10.4101 23.3859 13.0556 23.3859C15.7011 23.3859 16.0402 23.3859 17.0748 23.3252C17.8842 23.3093 18.685 23.1558 19.4429 22.8713C20.0904 22.6201 20.6785 22.2367 21.1696 21.7456C21.6607 21.2544 22.0441 20.6664 22.2954 20.0189C22.5803 19.2606 22.733 18.4611 22.7493 17.6496C22.7959 16.6075 22.8099 16.2771 22.8099 13.6305C22.8078 10.985 22.8078 10.6491 22.7471 9.61239ZM13.048 18.6008C10.2812 18.6008 8.03977 16.3594 8.03977 13.5926C8.03977 10.8257 10.2812 8.58431 13.048 8.58431C14.3763 8.58431 15.6502 9.11196 16.5894 10.0512C17.5286 10.9904 18.0563 12.2643 18.0563 13.5926C18.0563 14.9208 17.5286 16.1947 16.5894 17.1339C15.6502 18.0732 14.3763 18.6008 13.048 18.6008ZM18.2556 9.56689C18.1022 9.56703 17.9503 9.53693 17.8085 9.47829C17.6668 9.41965 17.538 9.33363 17.4295 9.22516C17.321 9.11669 17.235 8.98789 17.1764 8.84614C17.1177 8.70439 17.0876 8.55246 17.0878 8.39906C17.0878 8.24577 17.118 8.09398 17.1766 7.95236C17.2353 7.81073 17.3213 7.68205 17.4297 7.57366C17.5381 7.46527 17.6667 7.37928 17.8084 7.32062C17.95 7.26196 18.1018 7.23177 18.2551 7.23177C18.4084 7.23177 18.5601 7.26196 18.7018 7.32062C18.8434 7.37928 18.9721 7.46527 19.0805 7.57366C19.1889 7.68205 19.2748 7.81073 19.3335 7.95236C19.3922 8.09398 19.4224 8.24577 19.4224 8.39906C19.4224 9.04473 18.9002 9.56689 18.2556 9.56689Z"
                          fill="#6FCBDC"
                        />
                        <path
                          d="M13.0479 16.8459C14.8446 16.8459 16.3012 15.3893 16.3012 13.5926C16.3012 11.7959 14.8446 10.3394 13.0479 10.3394C11.2512 10.3394 9.79468 11.7959 9.79468 13.5926C9.79468 15.3893 11.2512 16.8459 13.0479 16.8459Z"
                          fill="#6FCBDC"
                        />
                      </svg>
                    </div>
                  </Link>
                  {/* telegram */}
                  <Link href="https://t.me/+62881082457238" target="_blank">
                    <div className="hover:scale-150 transition-all ease-out mt-[2px]">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="27"
                        height="27"
                        viewBox="0 0 27 27"
                      >
                        <path
                          fill="#6FCBDC"
                          d="M11.944 0A12 12 0 0 0 0 12a12 12 0 0 0 12 12a12 12 0 0 0 12-12A12 12 0 0 0 12 0a12 12 0 0 0-.056 0zm4.962 7.224c.1-.002.321.023.465.14a.506.506 0 0 1 .171.325c.016.093.036.306.02.472c-.18 1.898-.962 6.502-1.36 8.627c-.168.9-.499 1.201-.82 1.23c-.696.065-1.225-.46-1.9-.902c-1.056-.693-1.653-1.124-2.678-1.8c-1.185-.78-.417-1.21.258-1.91c.177-.184 3.247-2.977 3.307-3.23c.007-.032.014-.15-.056-.212s-.174-.041-.249-.024c-.106.024-1.793 1.14-5.061 3.345c-.48.33-.913.49-1.302.48c-.428-.008-1.252-.241-1.865-.44c-.752-.245-1.349-.374-1.297-.789c.027-.216.325-.437.893-.663c3.498-1.524 5.83-2.529 6.998-3.014c3.332-1.386 4.025-1.627 4.476-1.635z"
                        />
                      </svg>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomeFooter;
