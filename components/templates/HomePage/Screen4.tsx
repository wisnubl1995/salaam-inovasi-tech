import ButtonPrimary from "@/components/atoms/ButtonPrimary";
import TradingJournal from "/public/TradingJournal.png";
import Container from "@/components/atoms/Container";
import Paragraph from "@/components/atoms/Paragraph";
import HomePageSubHeader from "@/components/molecules/HomePageSubHeader";
import Image from "next/image";

import React from "react";

type Props = {
  click: any;
};

const Screen4 = (props: Props) => {
  return (
    <div className="rounded-[50px] lg:rounded-[75px] bg-mv-white-4 py-14 mt-10 lg:mt-32 z-30 overflow-visible p-6">
      <Container>
        <div className="w-full lg:w-1/3 text-center flex flex-col gap-4">
          <HomePageSubHeader textColor="Grow" text="With Us" />
          <Paragraph text="Fuel your development experiences with powerful tools at your fingertips. " />
        </div>

        <div className="mx-auto w-full px-6 lg:px-8">
          <div className="mx-auto mt-16 sm:mt-20 lg:mt-24 w-full ">
            <dl className="mx-auto lg:w-8/12 w-full flex-col gap-10 flex  items-center justify-center">
              <div className="flex flex-col lg:flex-row gap-2">
                <div className="relative pl-16">
                  <dt className="text-base font-semibold leading-7 text-gray-900">
                    <div className="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-indigo-600">
                      <svg
                        className="h-6 w-6 text-white"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M12 16.5V9.75m0 0l3 3m-3-3l-3 3M6.75 19.5a4.5 4.5 0 01-1.41-8.775 5.25 5.25 0 0110.233-2.33 3 3 0 013.758 3.848A3.752 3.752 0 0118 19.5H6.75z"
                        />
                      </svg>
                    </div>
                    UI/UX Design
                  </dt>
                  <dd className="mt-2 text-base leading-7 text-gray-600">
                    Elevate your digital presence with our expert UI/UX design
                    services. We specialize in creating visually stunning and
                    user-friendly interfaces that enhance the overall experience
                    of your applications and websites.
                  </dd>
                </div>
                <div className="relative pl-16">
                  <dt className="text-base font-semibold leading-7 text-gray-900">
                    <div className="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-indigo-600">
                      <svg
                        className="h-6 w-6 text-white"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"
                        />
                      </svg>
                    </div>
                    Webiste Development
                  </dt>
                  <dd className="mt-2 text-base leading-7 text-gray-600">
                    Craft engaging websites tailored to your brand, blending
                    aesthetic appeal with seamless functionality. Elevate user
                    experiences through responsive design, intuitive navigation,
                    and cutting-edge technologies.
                  </dd>
                </div>
                <div className="relative pl-16">
                  <dt className="text-base font-semibold leading-7 text-gray-900">
                    <div className="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-indigo-600">
                      <svg
                        className="h-6 w-6 text-white"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M16.5 10.5V6.75a4.5 4.5 0 10-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 002.25-2.25v-6.75a2.25 2.25 0 00-2.25-2.25H6.75a2.25 2.25 0 00-2.25 2.25v6.75a2.25 2.25 0 002.25 2.25z"
                        />
                      </svg>
                    </div>
                    Mobile Application Development
                  </dt>
                  <dd className="mt-2 text-base leading-7 text-gray-600">
                    Transform your ideas into innovative mobile applications
                    with our skilled development team. Whether youre targeting
                    iOS, Android, or cross-platform solutions, we deliver robust
                    and scalable mobile applications that meet the demands.
                  </dd>
                </div>
              </div>
              <div className="flex lg:flex-row flex-col gap-5 justify-center items-center w-full lg:w-8/12">
                <div className="relative pl-16">
                  <dt className="text-base font-semibold leading-7 text-gray-900">
                    <div className="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-indigo-600">
                      <svg
                        className="h-6 w-6 text-white"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"
                        />
                      </svg>
                    </div>
                    Software As A Service {"(SAAS)"}
                  </dt>
                  <dd className="mt-2 text-base leading-7 text-gray-600">
                    Experience innovative software solutions with our SaaS
                    offerings. Streamline operations, enhance productivity, and
                    stay ahead in the digital landscape. Effortless, scalable,
                    and tailored to meet your business goals.
                  </dd>
                </div>
                <div className="relative pl-16">
                  <dt className="text-base font-semibold leading-7 text-gray-900">
                    <div className="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-indigo-600">
                      <svg
                        className="h-6 w-6 text-white"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M16.5 10.5V6.75a4.5 4.5 0 10-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 002.25-2.25v-6.75a2.25 2.25 0 00-2.25-2.25H6.75a2.25 2.25 0 00-2.25 2.25v6.75a2.25 2.25 0 002.25 2.25z"
                        />
                      </svg>
                    </div>
                    Developer As A Service {"(DAAS)"}
                  </dt>
                  <dd className="mt-2 text-base leading-7 text-gray-600">
                    Unlock the power of dedicated developers for your projects.
                    Our Developer as a Service model provides skilled
                    professionals, ensuring efficient, high-quality development
                    to bring your visions to life.
                  </dd>
                </div>
              </div>
            </dl>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Screen4;
