import React, { useState } from "react";
import Subtitle from "@/components/atoms/Subtitle";
import H3 from "@/components/atoms/H3";
import Trending from "@/public/animate.gif";
import Trending1 from "@/public/trending.png";
import Paragraph from "@/components/atoms/Paragraph";
import Image from "next/image";
import HomePageSubHeader from "@/components/molecules/HomePageSubHeader";
import ButtonPrimary from "@/components/atoms/ButtonPrimary";
import Marquee from "react-fast-marquee";
import kotakijo from "@/public/icon/kotakijo.png";
import segitigakuning from "@/public/icon/segitigakuning.png";
import buletijo from "@/public/icon/buletijo.png";
import Layout from "../Layout";
import Lottie from "lottie-react";
import animate from "@/public/animate.json";
import pdc from "@/public/PDC-removebg-preview.png";
import pli from "@/public/PLI-removebg-preview.png";
import ppla from "@/public/PPLA-removebg-preview.png";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { api } from "@/lib/graphql/api";
import { ARTICLES, CLIENTS, PROJECTS } from "@/lib/graphql/query";
import Link from "next/link";
import BoxedLayout from "../BoxedLayout";

type Props = {
  click: any;
};

type ProjectCategory = {
  categoryName: string;
};

type Client = {
  clientName: string;
  slug: string;
  clientImage: any;
};

type Project = {
  projectName: string;
  slug: string;
  imageThumbnail: any;
  additionalImage: any;
  projectCategory: ProjectCategory;
  client: Client;
  projectDescription: string;
};

export type Clients = {
  clients: Client[];
  projects: Project[];
};

const TradingJournal = ({ clients, projects }: Clients) => {
  return (
    <div className="w-full min-h-screen my-4 flex flex-col">
      <div className="w-full lg:w-1/3 text-center flex flex-col gap-4  mx-auto pt-5">
        <HomePageSubHeader textColor="Our" text="Clients" />
        <Paragraph text="Meet our powerful Clients" />
      </div>
      <div className="h-full mx-auto font-QuicksandBold text-[36px] mt-3 flex justify-center items-center">
        {clients.map((client, index: number) => {
          return (
            <div
              key={index}
              className=" w-full flex items-center justify-center mt-14"
            >
              <Image
                src={client.clientImage.url}
                alt="client"
                className=""
                width={
                  client.clientName === "Pelopor Pratama Lancar Abadi"
                    ? 150
                    : 250
                }
                height={0}
              />
            </div>
          );
        })}
      </div>

      <BoxedLayout>
        <div className="flex flex-col gap-10  items-center">
          <div className="flex flex-col items-center py-24">
            <HomePageSubHeader textColor="Apps" text="We Develop" />
            <Paragraph text="Meet our powerful Clients" />
          </div>
          {projects.slice(0, 2).map((project, index: number) => {
            return (
              <div
                key={index}
                className={`flex gap-5 justify-between w-full items-center mb-10 ${
                  index % 2 == 0 ? "flex-row" : "flex-row-reverse"
                }`}
              >
                <div className="rounded-lg overflow-hidden shadow-md">
                  <img
                    src={project.imageThumbnail.url}
                    width="300"
                    className="object-cover"
                    alt="porject"
                  />
                </div>
                <div className="grid gap-2">
                  <div className="w-full">
                    <div className="font-QuicksandBold">
                      <h1 className="text-2xl">{project.projectName}</h1>
                    </div>
                  </div>

                  <div>
                    <h3>{project.projectDescription}</h3>
                  </div>

                  <div>
                    <Link href={`/expertise/${project.slug}`}>
                      <button className="px-5 py-[1px] bg-mv-secondary-1 text-white rounded-xl hover:bg-mv-primary-1 hover:scale-110 transition-all ease-out duration-200">
                        Details
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </BoxedLayout>
      <div className="mt-10 flex justify-center items-center">
        <Link href={"/expertise"}>
          <button className="px-5 py-3 bg-mv-secondary-1 text-white rounded-full hover:bg-mv-primary-1 hover:scale-110 transition-all ease-out duration-200">
            More Projects
          </button>
        </Link>
      </div>
      <Layout>
        <div className="flex flex-col-reverse lg:flex-row items-center justify-center p-6 lg:px-[160px]  lg:mt-0">
          <div className="w-full lg:w-6/12 flex flex-col gap-4">
            <HomePageSubHeader
              textColor="Connect"
              text="with like minded people"
            />
            <Paragraph text="Explore the Digital Horizon, where connections transcend boundaries, forging relationships that redefine the essence of meaningful connections in our interconnected world!" />
            <div className="w-full">
              <Link href={"/contact-us"}>
                <ButtonPrimary text="Say Hi!" />
              </Link>
            </div>
          </div>
          <div className="w-full lg:w-6/12 flex justify-center pb-10 lg:pb-0">
            <Lottie animationData={animate} />
          </div>
        </div>
      </Layout>
    </div>
  );
};

export default TradingJournal;
