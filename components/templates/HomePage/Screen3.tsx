import React from "react";
import HomePageSubHeader from "@/components/molecules/HomePageSubHeader";
import ButtonPrimary from "@/components/atoms/ButtonPrimary";
import Paragraph from "@/components/atoms/Paragraph";
import buku from "@/public/images/book.png";
import Image from "next/image";
import { useRouter } from "next/router";
import Lottie from "lottie-react";
import secure from "@/public/secure.json";

type Props = {};

const Screen3 = (props: Props) => {
  const router = useRouter();
  return (
    <div className="flex flex-col lg:flex-row w-full justify-center my-4 p-6">
      <div className="w-full lg:w-1/3  rounded-xl">
        <Lottie animationData={secure} />
      </div>
      <div className=" w-full lg:w-1/2 lg:p-20 pt-10 flex flex-col gap-5">
        <HomePageSubHeader
          textColor="Your Gateway"
          text=" to Secure Robust IT Development"
        />
        <Paragraph text="Empowering Your Digital Future with Safeguarding Every Byte, Every Moment, Every Connection Development. Trust FortifyTech for Unrivaled Protection and Innovation." />
        <div className="w-full" onClick={() => router.push("/aboutus")}>
          <ButtonPrimary text="About Us" />
        </div>
      </div>
    </div>
  );
};

export default Screen3;
