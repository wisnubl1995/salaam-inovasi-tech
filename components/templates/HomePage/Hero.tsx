import React from "react";
import { motion } from "framer-motion";
import Image from "next/image";
import GradientLine from "/public/gradientline.png";
import Iphone from "public/icon/hero.svg";
import Link from "next/link";
import Paragraph from "@/components/atoms/Paragraph";
import OneLiner from "@/components/atoms/OneLiner";
import ButtonPrimary from "@/components/atoms/ButtonPrimary";
import brain from "@/public/images/barin.png";
import money from "@/public/desktop.svg";
import bulet from "@/public/images/bulet.png";
import buletfil from "@/public/images/buletfill.png";
import segitiga from "@/public/images/segitiga.png";
import segitigafil from "@/public/images/segitigafill.png";
import arrow from "@/public/images/arrow.png";
import arrowhead from "@/public/images/arrowhead.png";
import Lottie from "lottie-react";
import mainHero from "@/public/hero.json";

type Props = {
  click: any;
};

const Hero = (props: Props) => {
  const icon = {
    hidden: {
      pathLength: 0,
      opacity: 0,
    },
    visible: {
      pathLength: 1,
      opacity: 1,
    },
  };
  return (
    <div className="flex flex-col bg-hero bg-bottom bg-cover bg-no-repeat min-h-screen items-center rounded-b-[75px]">
      <div className="flex flex-col justify-center items-center mt-10 lg:pt-24  relative">
        <div className="absolute hidden lg:block left-[-50%] top-[0%]">
          <svg
            width="1024"
            height="250"
            viewBox="0 0 1296 342"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <motion.path
              d="M-36.5479 340.084C-33.1199 235.96 3.81654 24.6253 124.138 12.2846C274.541 -3.14125 583.058 340.084 804.163 340.084C1025.27 340.084 1245.09 50.8494 1279.79 16.1412"
              stroke="url(#paint0_linear_147_4)"
              strokeOpacity="0.4"
              strokeWidth="3"
              variants={icon}
              initial="hidden"
              animate="visible"
              transition={{ duration: 1.5 }}
            />
            <motion.path
              d="M-36.5479 340.084C-33.1199 235.96 3.81654 24.6253 124.138 12.2846C274.541 -3.14125 583.058 340.084 804.163 340.084C1025.27 340.084 1245.09 50.8494 1279.79 16.1412"
              stroke="url(#paint1_linear_147_4)"
              strokeWidth="3"
              variants={icon}
              initial="hidden"
              animate="visible"
              transition={{ duration: 1.5 }}
            />
            <motion.path
              d="M1282.99 9.5195C1284.44 8.98537 1285.92 10.2147 1285.66 11.7323L1283.12 26.5602C1282.86 28.0778 1281.06 28.7454 1279.87 27.762L1268.3 18.1529C1267.11 17.1695 1267.44 15.2725 1268.88 14.7384L1282.99 9.5195Z"
              fill="#3E449B"
              variants={icon}
              initial={{ translateX: -50, translateY: 50, opacity: 0 }}
              animate={{ translateX: 0, translateY: 0, opacity: 1 }}
              transition={{ duration: 0.3, delay: 1.2 }}
            />
            <defs>
              <linearGradient
                id="paint0_linear_147_4"
                x1="2138.71"
                y1="158.512"
                x2="96.5551"
                y2="76.4773"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor="#6FCBDC" />
                <stop offset="1" stopColor="#6FCBDC" stopOpacity="0" />
              </linearGradient>
              <linearGradient
                id="paint1_linear_147_4"
                x1="1279.79"
                y1="11.7797"
                x2="1005.93"
                y2="263.239"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor="#3E449B" />
                <stop offset="1" stopColor="#3E449B" stopOpacity="0" />
              </linearGradient>
            </defs>
          </svg>
        </div>

        <div className="flex flex-row gap-5">
          <div className="text-mv-primary-1  relative">
            <motion.div
              initial={{ opacity: 0, translateX: -10 }}
              whileInView={{ opacity: 1, translateX: 0 }}
              viewport={{ once: true }}
              transition={{ duration: 0.5 }}
            >
              <OneLiner text="Salaam Inovasi" />
            </motion.div>
            <motion.div
              initial={{ opacity: 0, scale: 0 }}
              whileInView={{ opacity: 1, scale: 1 }}
              transition={{ duration: 0.5 }}
              className="absolute top-[-20px] left-[-20px]"
            >
              <Image
                className="w-4/12"
                src={segitigafil}
                alt=""
                width={1000}
                height={10}
              />
            </motion.div>
            <motion.div
              initial={{ opacity: 0, scale: 0 }}
              whileInView={{ opacity: 1, scale: 1 }}
              transition={{ duration: 0.5, delay: 0.5 }}
              className="absolute bottom-[-30px] left-[-30px]"
            >
              <Image
                className="w-4/12"
                src={segitiga}
                alt=""
                width={100}
                height={0}
              />
            </motion.div>
            <motion.div
              initial={{ opacity: 0, scale: 0 }}
              whileInView={{ opacity: 1, scale: 1 }}
              transition={{ duration: 0.5, delay: 0.3 }}
              className="absolute top-[-20px] left-[100px]"
            >
              <Image
                className="w-3/12"
                src={buletfil}
                alt=""
                width={100}
                height={0}
              />
            </motion.div>
          </div>

          <div className="col-span-2 flex items-center ml-2 relative">
            <motion.div
              initial={{ opacity: 0, scale: 0 }}
              whileInView={{ opacity: 1, scale: 1 }}
              transition={{ duration: 1.5, delay: 0.5 }}
              className="rounded-full border-mv-secondary-1 border w-14 h-14 flex items-center justify-center animate-ping"
            ></motion.div>
            <motion.div
              initial={{ opacity: 0, scale: 0 }}
              whileInView={{ opacity: 1, scale: 1 }}
              transition={{ duration: 0.5, delay: 0.5 }}
              className="rounded-full bg-mv-secondary-1 w-12 h-12 absolute left-1"
            >
              <motion.div
                whileHover={{ rotate: 360 }}
                className="w-[80px] absolute top-[-15px] left-[-12px]"
              >
                <Image
                  className="w-[68px] h-[68px] mt-1 object-cover"
                  src={brain}
                  alt=""
                  width={100}
                  height={0}
                />
              </motion.div>
            </motion.div>
          </div>
        </div>

        <div className="flex flex-row-reverse gap-5">
          <motion.div
            initial={{ opacity: 0, translateX: 10 }}
            whileInView={{ opacity: 1, translateX: 0 }}
            viewport={{ once: true }}
            transition={{ duration: 0.5 }}
            className="text-mv-secondary-1"
          >
            <OneLiner text="Teknologi" />
          </motion.div>
          <div className="flex items-center relative">
            <div className="rounded-full border-mv-primary-4 border w-14 h-14 flex items-center justify-center"></div>
            <div className="rounded-full bg-mv-primary-4 w-12 h-12 absolute left-1">
              <div className="w-[70px] lg:w-[100px] absolute top-[-15px] left-[-11px] lg:top-[-15px] lg:left-[-13px]">
                <Image
                  className="w-[73px] h-[73px] object-contain scale-75"
                  src={money}
                  alt=""
                  width={100}
                  height={0}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="flex justify-center p-6 w-full md:w-1/2 lg:w-1/3 text-center">
        <Paragraph
          text="Salaam Inovasi Teknologi: Human Touch in the Digital Revolution, Guiding a Progressive and Harmonious Future for a Better World.
          "
        />
      </div>

      <div className="flex flex-col w-full justify-end">
        <div className="flex gap-4 ">
          <div className="flex justify-end w-1/3 relative rounded-bl-[75px] overflow-hidden ">
            <div className="rounded-full hidden lg:flex bg-mv-secondary-1 w-24 h-24"></div>
          </div>
          <motion.div
            initial={{ opacity: 0, translateY: 50 }}
            whileInView={{ opacity: 1, translateY: 0 }}
            transition={{ duration: 0.5 }}
            className="flex justify-center  overflow-visible w-full mt-16 lg:mt-0 lg:w-1/3 relative z-20"
          >
            <div className="scale-150">
              <Lottie animationData={mainHero} />
            </div>
          </motion.div>
          <div className="w-1/3 h-96 overflow-hidden"></div>
        </div>
      </div>
    </div>
  );
};

export default Hero;
