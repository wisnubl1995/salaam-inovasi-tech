import Container from "@/components/atoms/Container";
import OneLiner from "@/components/atoms/OneLiner";
import Subtitle from "@/components/atoms/Subtitle";
import Card from "@/components/molecules/Card";
import React from "react";
import HomePageSubHeader from "@/components/molecules/HomePageSubHeader";
import lampu from "@/public/icon/lampu.svg";
import connect from "@/public/icon/connect.svg";
import grow from "@/public/icon/grow.svg";

type Props = {};

const Screen5 = (props: Props) => {
  return (
    <div className="bg-mv-gray-1 -mt-20 pt-10 z-20 min-h-screen rounded-b-[50px] lg:rounded-b-[75px] text-white">
      <Container>
        <div className="flex pt-20 lg:py-20">
          <HomePageSubHeader text="Our Values" />{" "}
          <span className="text-mv-secondary-3">
            <HomePageSubHeader text="." />
          </span>
        </div>
        <div className="flex flex-col lg:flex-row">
          <Card
            image={lampu}
            title="Innovation."
            paragraph="We drive progress through inventive solutions, embracing new ideas to exceed expectations and stay ahead in a rapidly evolving digital landscape."
          />
          <Card
            image={connect}
            title="Connection."
            paragraph="Building strong relationships, we prioritize open communication and understanding, fostering meaningful connections with clients and end-users alike."
          />
          <Card
            image={grow}
            title="Integrity."
            paragraph="Our foundation is integrity. We uphold the highest ethical standards, ensuring transparent communication and delivering reliable solutions with honesty and trust."
          />
        </div>
        <div className="w-full lg:w-1/2 text-center my-12 px-6 lg:px-0 font-QuicksandMedium text-3xl">
          <Subtitle
            text="These core values of Innovation, Connection, and Integrity shape our company culture and guide our actions at Salaam Inovasi Teknologi. 
By embracing these values, we aim to create an environment that promotes continuous integrating, collaboration, and developemnt, allowing collabolators to achieve long-term success"
          />
        </div>
      </Container>
    </div>
  );
};

export default Screen5;
