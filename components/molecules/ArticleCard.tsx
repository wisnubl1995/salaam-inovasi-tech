import Image from "next/image";
import React from "react";
import placeholder from "@/public/images/placeholder-learn.png";
import { useRouter } from "next/router";
import ButtonPrimary from "../atoms/ButtonPrimary";
import Link from "next/link";
import Paragraph from "../atoms/Paragraph";

type Props = {
  full?: boolean;
  name: string;
  slug: string;
  url: any;
  tag: string;
  content: any;
  date: string;
  click?: any;
};

const ArticleCard = (props: Props) => {
  if (!props.full) {
    return (
      <>
        <div className="w-full flex flex-col justify-between  mt-1 h-full rounded-xl overflow-hidden ">
          <div>
            <div className="w-full overflow-hidden relative">
              <figure
                onClick={props.click}
                className="w-full  rounded-xl overflow-hidden cursor-pointer"
              >
                <Image
                  className="lg:w-[300px] w-full aspect-square object-cover"
                  src={props.url}
                  width={1000}
                  height={10}
                  alt=""
                />
              </figure>
              <div className="absolute top-5 left-5">
                <button className="text-white bg-black px-2 rounded-xl py-1 font-QuicksandBold text-[10px] ">
                  {props.tag}
                </button>
              </div>
            </div>
            <h3
              onClick={props.click}
              className="font-QuicksandSemibold text-[16px] leading-none py-4 cursor-pointer"
            >
              {props.name}
            </h3>
            <h3 className="font-QuicksandRegular text-[10px]">{props.date}</h3>
          </div>
          <div>
            <div>
              <Paragraph text={props.content} />
            </div>
            <div className="mt-7 w-full cursor-pointer">
              <Link
                className="w-full py-4 text-white flex justify-center items-center bg-mv-primary-1 hover:bg-mv-secondary-5 transition-all ease-out"
                href={`article/${props.slug}`}
              >
                Read More
              </Link>
            </div>
          </div>
        </div>
      </>
    );
  }
  return (
    <div
      onClick={props.click}
      className="w-full flex flex-row cursor-pointer mt-1"
    >
      <div className="w-[300px] overflow-hidden relative">
        <figure className="w-[280px] h-[280px] bg-slate-400 overflow-hidden">
          <Image
            className="w-full h-full object-cover"
            src={props.url}
            width={1000}
            height={10}
            alt=""
          />
        </figure>
        <div className="absolute top-5 left-5">
          <button className="text-white bg-black px-5 rounded-xl py-2 font-QuicksandBold text-[10px] ">
            {props.tag}
          </button>
        </div>
      </div>
      <div className="w-6/12 ml-4 flex flex-col justify-between">
        <div>
          <h3 className="font-QuicksandSemibold text-[20px] leading-normal">
            {props.name}
          </h3>
          <h3 className="font-QuicksandRegular text-[15px]">{props.date}</h3>
          <p className="md:text-xs lg:text-sm font-QuicksandRegular my-1 tracking-wide leading-2">
            {props.content}
          </p>
        </div>
        <div>
          <ButtonPrimary text="Read More" />
        </div>
      </div>
    </div>
  );
};

export default ArticleCard;
