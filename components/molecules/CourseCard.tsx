import Image from "next/image";
import React from "react";
import placeHolder from "@/public/images/placeholder-learn.png";
import ButtonPrimary from "../atoms/ButtonPrimary";
import ProgressBar from "./ProgressBar";
import { useRouter } from "next/router";

type Props = {
  image?: any;
  id?: string;
  title?: string;
  desc?: string;
  clas?: any;
  link?: any;
};

const CourseCard = ({ image, id, title, desc, clas, link }: Props) => {
  const router = useRouter();

  return (
    <div className="bg-white w-10/12 shadow-xl mx-auto relative flex flex-col py-[28px] px-[34px] rounded-[20px]">
      <div className="flex flex-col w-full items-center lg:flex-row gap-10">
        <div className="flex justify-center w-full lg:w-4/12">
          <Image className="w-full" src={image ? image : placeHolder} alt="" />
        </div>
        <div className="flex flex-col gap-3 w-full lg:w-8/12 justify-end">
          <div className="flex flex-col gap-2">
            <h3 className="font-QuicksandSemibold text-[16px] text-[#B3B3B3] leading-6">
              {`Total Topics of Class = ${clas}`}
            </h3>
            <h3 className="font-QuicksandBold text-[24px]">{title}</h3>
            <p className="font-QuicksandMedium text-[16px] lg:w-7/12">{desc}</p>
          </div>
          <div onClick={link} className="lg:w-4/12">
            <ButtonPrimary
              custom
              style="px-5 text-[12px] py-1"
              text="Start Course"
            />
          </div>
        </div>
        <div className="flex lg:absolute top-10 right-10">
          <ProgressBar />
        </div>
      </div>
    </div>
  );
};

export default CourseCard;
