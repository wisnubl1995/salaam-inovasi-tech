import React from "react";
import Paragraph from "../atoms/Paragraph";

type Props = {
  rightJustify?: boolean;
  title?: string;
  text?: any;
};

const AboutUsCard = ({ rightJustify, title, text }: Props) => {
  if (rightJustify) {
    return (
      <div className="flex flex-col p-4 gap-4 text-right items-end">
        <div className="font-QuicksandBold text-2xl">{title}</div>
        <div className="">
          <Paragraph text={text} />
        </div>
      </div>
    );
  }
  return (
    <div className="flex flex-col p-4 gap-4">
      <div className="font-QuicksandBold text-2xl">{title}</div>
      <div className="">
        <Paragraph text={text} />
      </div>
    </div>
  );
};

export default AboutUsCard;
