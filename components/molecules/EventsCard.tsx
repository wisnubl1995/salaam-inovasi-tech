import React from "react";
import EventMark from "../atoms/EventMark";
import { AiOutlineCalendar } from "react-icons/ai";
import Image from "next/image";
import { useRouter } from "next/router";

type Props = {
  title: string;
  date?: string;
  buttonText: string;
  image?: any;
  description?: any;
  link?: any;
};

const EventsCard = ({
  date,
  title,
  buttonText,
  image,
  description,
  link,
}: Props) => {
  const router = useRouter();
  return (
    <div className="flex flex-col justify-between gap-1 p-2  lg:h-full">
      <div>
        {!image ? (
          <div className="bg-mv-secondary-6 relative w-full lg:h-[15vw] h-[25vw] rounded-xl">
            <EventMark />{" "}
          </div>
        ) : (
          <div className="w-full lg:h-[15vw] h-[25vw] rounded-xl relative overflow-hidden">
            <Image
              className="w-full h-full object-cover"
              src={image}
              alt={title}
              width={1000}
              height={100}
            />
            <EventMark />
          </div>
        )}
        <div className="px-2 py-1 flex flex-col gap-1">
          <div className="">
            <h3 className="font-QuicksandSemibold lg:text-[16px] text-[12px]">
              {title}
            </h3>
          </div>
          <div className="md:text-xs lg:text-sm font-QuicksandRegular my-1 tracking-wide leading-2">
            <p>{date}</p>
            <p>{description}</p>
          </div>
        </div>
      </div>
      <div className="">
        <button
          className="w-full bg-mv-primary-3 cursor-pointer text-white rounded-full shadow-md py-2 px-4 hover:bg-mv-secondary-1 transition-all ease-out"
          onClick={() => {
            router.push(link ? link : `#`);
          }}
        >
          {buttonText}
        </button>
      </div>
    </div>
  );
};

export default EventsCard;
