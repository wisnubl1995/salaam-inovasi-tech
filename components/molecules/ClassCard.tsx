import Image from "next/image";
import React from "react";
import placeHolder from "@/public/images/placeholder-learn.png";
import play from "@/public/icon/pl.svg";
import ButtonPrimary from "../atoms/ButtonPrimary";
import ProgressBar from "./ProgressBar";
import { useRouter } from "next/router";

type Topic = {
  topicName: string;
  description: string;
  slug: string;
  class: any;
  courses: any;
};

type Props = {
  image?: any;
  id?: string;
  title?: string;
  desc?: string;
  clas?: any;
  link?: any;
  topics?: any;
};

const ClassCard = ({ image, id, title, desc, clas, link, topics }: Props) => {
  const router = useRouter();

  return (
    <div className="bg-white w-10/12 mx-auto shadow-xl relative flex flex-col py-[28px] px-[34px] rounded-[20px]">
      <div className="flex flex-col w-full items-center lg:flex-row gap-10">
        <div className="flex lg:absolute top-10 right-10">
          <ProgressBar />
        </div>
        <div className="flex justify-center w-full lg:w-4/12">
          <Image className="w-full" src={image ? image : placeHolder} alt="" />
        </div>
        <div className="flex flex-col gap-5 w-full lg:w-8/12 justify-end">
          <div className="flex flex-col gap-3">
            <h3 className="font-QuicksandSemibold text-[16px] text-[#B3B3B3] leading-6">
              {`Total Topics of Class = ${clas}`}
            </h3>
            <h3 className="font-QuicksandBold text-[24px] w-8/12 ">{title}</h3>
            <p className="font-QuicksandMedium text-[16px] lg:w-7/12">{desc}</p>
          </div>
          <div onClick={link} className="lg:w-4/12">
            <ButtonPrimary
              custom
              style="px-5 text-[12px] py-1"
              text="Start Course"
            />
          </div>
        </div>
      </div>
      <div className="flex flex-col w-full">
        <h3 className="pl-0 lg:pl-10 py-5 text-[16px] font-QuicksandSemibold text-[#B3B3B3]">
          Course Outline:
        </h3>
        <div className="flex flex-col lg:flex-row lg:flex-wrap lg:gap-8 gap-5 lg:px-10">
          {topics.map((v: any, i: any) => {
            return (
              <div
                onClick={() => {
                  router.push(`/learn/${id}/${v.slug}`);
                }}
                className="flex flex-row items-center gap-2 cursor-pointer"
                key={i}
              >
                <h3>{i + 1}</h3>
                <Image src={play} alt="" />
                <h1>{v.courseName}</h1>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ClassCard;
