import React from "react";
import Navbar from "@/components/organisms/Navbar";
import Hero from "@/components/templates/HomePage/Hero";
import Screen2, { Clients } from "@/components/templates/HomePage/Screen2";
import Screen3 from "../templates/HomePage/Screen3";
import Screen4 from "../templates/HomePage/Screen4";
import Ourmindset from "../templates/HomePage/Screen5";
import Screen6 from "../templates/HomePage/Screen6";
import HomeFooter from "../templates/HomePage/HomeFooter";
import Layout from "../templates/Layout";
import Seo from "../templates/Seo";
import { useRouter } from "next/router";
import { api } from "@/lib/graphql/api";
import { CLIENTS } from "@/lib/graphql/query";

type Props = {};

const Homepage = ({ clients, projects, logo }: any) => {
  const router = useRouter();
  const contact = (e: any) => {
    e.preventDefault;
    router.push(`/contact-us`);
  };

  return (
    <div className="bg-white relative">
      <div className="w-full bg-mv-white-4">
        <Layout>
          <Navbar logo={logo} />
        </Layout>
      </div>
      <div className="w-full bg-mv-white-4">
        <Layout>
          <Hero click={contact} />
        </Layout>
      </div>
      <Screen2 clients={clients} projects={projects} />
      <Layout>
        <Screen3 />
      </Layout>
      <div className="flex flex-col">
        <Screen4 click={contact} />
        <Ourmindset />
        <Screen6 click={contact} />
      </div>

      <HomeFooter />
    </div>
  );
};

export default Homepage;
