import React from "react";

type Props = {
  text: string;
};

const Paragraph = ({ text }: Props) => {
  return <p className="text-xs lg:text-lg font-QuicksandRegular tracking-wide leading-1 opacity-75">{text}</p>;
};

export default Paragraph;
