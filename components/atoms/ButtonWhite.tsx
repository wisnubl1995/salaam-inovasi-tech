import React from "react";

type Props = {
  text: string;
};

const ButtonWhite = ({ text }: Props) => {
  return (
    <button className="w-auto bg-mv-white-1 text-mv-primary-1 rounded-full shadow-md py-4 px-8 font-QuicksandSemibold  hover:bg-mv-secondary-1 hover:text-mv-white-1 transition-all ease-out min-[320px]:py-2 min-[320px]:px-4 min-[320px]:text-[11px] min-[480px]:py-3 min-[480px]:px-6 lg:text-lg">
      {text}
    </button>
  );
};

export default ButtonWhite;
