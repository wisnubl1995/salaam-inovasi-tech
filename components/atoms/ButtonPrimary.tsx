import React from "react";

type Props = {
  text: string;
  custom?: boolean;
  style?: any;
  disable?: any;
};

const ButtonPrimary = ({ text, custom, style, disable }: Props) => {
  if (custom === true) {
    return (
      <button
        className={
          style +
          " " +
          "w-auto bg-mv-primary-3 text-white rounded-full shadow-md hover:bg-mv-secondary-1 transition-all ease-out"
        }
        disabled={disable}
      >
        {text}
      </button>
    );
  }
  return (
    <button className="bg-mv-primary-3 text-white rounded-full shadow-md lg:py-2 lg:px-8 hover:bg-mv-secondary-1 transition-all ease-out min-[320px]:py-2 min-[320px]:px-4 min-[320px]:text-[11px] min-[480px]:py-3 min-[480px]:px-6 lg:text-lg">
      {text}
    </button>
  );
};

export default ButtonPrimary;
