import React from "react";

type Props = {};

const EventMark = (props: Props) => {
  return (
    <div className="bg-eventMark bg-cover bg-bottom w-6 h-8 absolute top-0 left-4 rounded-t-md"></div>
  );
};

export default EventMark;
