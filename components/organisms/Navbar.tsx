import Image from "next/image";
import Logo from "/public/logo-landscape.png";
import React, { useEffect } from "react";
import Link from "next/link";
import Subtitle from "../atoms/Subtitle";
import Button from "../atoms/ButtonPrimary";
import { useRouter } from "next/router";
import { api } from "@/lib/graphql/api";

type Props = {
  className?: any;
  logo: any;
};

const Navbar = (props: Props) => {
  const router = useRouter();
  const menuBtnRef = React.useRef(null);
  const mobileMenuRef = React.useRef(null);

  const login = (e: any) => {
    e.preventDefault;
    router.push(`${process.env.NEXT_PUBLIC_METAVULUS}`);
  };

  // Use the useEffect hook to toggle the classes when the button is clicked
  useEffect(() => {
    const menuBtn: any = menuBtnRef.current;
    const mobileMenu: any = mobileMenuRef.current;

    const handleClick = () => {
      menuBtn.classList.toggle("is-active");
      mobileMenu.classList.toggle("is-active");
    };

    menuBtn.addEventListener("click", handleClick);

    // Clean up the event listener when the component is unmounted
    return () => {
      menuBtn.removeEventListener("click", handleClick);
    };
  }, []);

  return (
    <div
      className={`flex justify-between items-center py-5 px-6 lg:px-[72px] z-60 ${props.className}`}
    >
      <div className="w-8/12">
        <Link href={"/"}>
          <Image
            className="lg:w-2/12 w-6/12"
            src={props.logo}
            alt=""
            width={1000}
            height={0}
          />
        </Link>
      </div>
      <div className="w-6/12 flex flex-row justify-between">
        <div className="hidden gap-5 items-center lg:flex">
          <Link href={"/aboutus"} className="">
            <Subtitle menu={true} text="About" />
          </Link>
          <Link href={"/expertise"} className="">
            <Subtitle menu={true} text="Expertise" />
          </Link>
          <Link href={"/career"} className="">
            <Subtitle menu={true} text="Career" />
          </Link>
          <Link href={"/article"} className="">
            <Subtitle menu={true} text="News" />
          </Link>
        </div>
        <div className="flex w-full justify-end mr-2 lg:mr-0 ">
          <Link href={"/contact-us"}>
            <Button text="Contact Us!" />
          </Link>
        </div>
      </div>
      <div className="flex lg:hidden">
        <button ref={menuBtnRef} className="w-6 hamburger  lg:hidden">
          <div className="bar "></div>
        </button>
        <nav className="mobile-container">
          <ul ref={mobileMenuRef} className="mobile-nav">
            <Link href={"/aboutus"} className="">
              <Subtitle menu={true} text="About Us" />
            </Link>
            <Link href={"/expertise"} className="">
              <Subtitle menu={true} text="Expertise" />
            </Link>
            <Link href={"/career"} className="">
              <Subtitle menu={true} text="Career" />
            </Link>
            <Link href={"/article"} className="">
              <Subtitle menu={true} text="News" />
            </Link>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
